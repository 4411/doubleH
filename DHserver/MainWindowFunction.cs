﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using System.Net;
using System.Net.Sockets;
using DoubleH.Utility.IO;
using Table = FCNS.Data.Table;
using System.Data;
using FSLib.IPMessager;

namespace DHserver
{
    public partial class MainWindow : Window
    {

        private void StartNet()
        {
            NormalUtility nu = new NormalUtility();
            lanClient = nu.InitNet("doubleH", "Server");
            if (lanClient != null)
            {
                imageHost.Source = new BitmapImage(new Uri(@"Pack://application:,,,/DHserver;component/Images/right_24.png"));
                lanClient.OnlineHost.HostOnline += (ss, ee) => OnlineHost(ee);
                lanClient.OnlineHost.HostOffline += (ss, ee) => OfflineHost(ee);
            }
            else
                imageHost.Source = new BitmapImage(new Uri(@"Pack://application:,,,/DHserver;component/Images/error_24.png"));
        }

        private void OnlineHost(FSLib.IPMessager.Entity.OnlineHost.HostEventArgs e)
        {
            StationStruct obj= new StationStruct();

            obj.Host = e.Host;
            obj.LastLoginDateTime = DateTime.Now;
            obj.OperatorerName = e.Host.HostSub.UserName;
            obj.StationName = e.Host.NickName;
            obj.StationType = e.Host.HostSub.HostName;
            obj.StationIP = e.Host.HostSub.ToString();

            allStation.Add(obj);
        }

        private void OfflineHost(FSLib.IPMessager.Entity.OnlineHost.HostEventArgs e)
        {
            StationStruct obj = allStation.FirstOrDefault(f => (f.Host == e.Host));
            if (obj != null)
                allStation.Remove(obj);
        }

        private void StopNet()
        {
            if (lanClient == null)
                return;

            lanClient.OffLine();
            lanClient.Dispose();
            lanClient = null;
        }



        private void StartWeb()
        {
            string processPath = FCNS.Data.DbDefine.baseDir + "WebDev.WebServer40.exe";
            string webPath = FCNS.Data.DbDefine.baseDir + "web";
            if (!File.Exists(processPath) || !Directory.Exists(webPath))
            {
                MessageWindow.Show("Web服务器丢失");
                imageWeb.Source = new BitmapImage(new Uri(@"Pack://application:,,,/DHserver;component/Images/error_24.png"));
            }
            else
            {
                Process.Start(processPath, " /path:\"" + webPath + "\"  /port:" + DoubleHConfig.AppConfig.WebPort);
                imageWeb.Source = new BitmapImage(new Uri(@"Pack://application:,,,/DHserver;component/Images/right_24.png"));
            }
        }

        private void StopWeb()
        {
            foreach (Process pro in Process.GetProcessesByName("WebDev.WebServer40"))
                pro.Kill();
        }
    }

    internal class StationStruct
    {
        public FSLib.IPMessager.Entity.Host Host { get; set; }
        public string StationType { get; set; }
        public string StationName { get; set; }
        public string StationIP { get; set; }
        public string StationStatus { get; set; }
        public DateTime LastLoginDateTime { get; set; }
        public string OperatorerName { get; set; }
    }
}
