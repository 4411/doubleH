﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;

namespace SalesOrderS
{
    class SalesOrderSExt : Plugin
    {
        private IPluginHost ihost = null;
        MainWindow window = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            ihost = host;
            window = new MainWindow();

            Table.SalesOrderS.EnumFlag flag = Table.SalesOrderS.EnumFlag.销售订单;
            switch(TableText)
            {
                case DataTableText.销售退货单:
                flag = Table.SalesOrderS.EnumFlag.销售退货单;
                    break;
                
                case DataTableText.赠品出库单:
                    flag = Table.SalesOrderS.EnumFlag.赠品出库单;
                    break;

                case DataTableText.销售询价单:
                    flag = Table.SalesOrderS.EnumFlag.销售询价单;
                    break;
            }
            window.Init(flag, DataTable as Table.SalesOrderS);
            window.Owner = host.WorkAreaWindow;
            window.Show();
            //window.Closed += (ss, ee) => { ihost.WorkAreaWindow.RefreshDataGrid(); };
            //Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();

            //ihost.WorkAreaWindow.RefreshDataGrid();
        }
    }
}
