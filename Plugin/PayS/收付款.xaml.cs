﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility;
using System.Collections.ObjectModel;
using DoubleH.Utility.Configuration;

namespace PayS
{
    /// <summary>
    /// ShouKuan.xaml 的交互逻辑
    /// </summary>
    public partial class ShouKuan : Window
    {
        public ShouKuan()
        {
            InitializeComponent();
        }

        double selectMoney = 0;
        DataTableText TableText;
        ObservableCollection<Table.PayS> payS;
        Table.PayS selected = null;

        public void Init(DataTableText tableText, Table.PayS obj)
        {
            InitVar(tableText, obj);
            InitEvent();
        }

        private void GetBill()
        {
            payS = Table.PayS.GetBill(customerSinfo1.SelectedObjectId, TableText == DataTableText.收款单 ? Table.PayS.EnumFlag.收款单 : Table.PayS.EnumFlag.付款单);

            dataGridObject.ItemsSource = payS;
            double d = 0;
            foreach (Table.PayS row in payS)
            {
                row.RealMoney = row.Money;
                d += row.Money;
            }

            label1Due.Content = Math.Round(d, Table.SysConfig.SysConfigParams.DecimalPlaces);
            ckbSelectedAll_Checked(null, null);
            buttonOK.IsEnabled = dataGridObject.HasItems;
            if (buttonOK.IsEnabled)
            {
                double corSMoney = Table.CorS.GetMoney(customerSinfo1.SelectedObjectId);
                labelCorSMoney.Content = corSMoney;

                if ((corSMoney <0 && TableText == DataTableText.付款单) || (corSMoney > 0 && TableText == DataTableText.收款单))
                    checkBoxCorSMoney.Visibility = Visibility.Visible;
            }
        }

        private void InitVar(DataTableText tableText, Table.PayS obj)
        {
            dataGridObject.ShowHeaderMenu = false;
            TableText = tableText;
            uCUniqueSBank.Init(Table.UniqueS.EnumFlag.支付方式);

            if (obj != null)
            //    SelectedCorS(-1);
            //else
            {
                //SelectedCorS(obj.CorSId);
                customerSinfo1.SelectedObjectId = obj.CorSId;
                GetBill();
                uCUniqueSBank.SelectedObjectId = obj.BankId;
            }
            label1.Content = this.Title = tableText.ToString();
        }

        private void InitEvent()
        {
            dataGridObject.SelectedCellsChanged += (ss, ee) => SelectedCell();
            buttonOK.Click += (ss, ee) => Save();
            customerSinfo1.CorSChanged += (ee) => GetBill();
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
        }

        private void SelectedCell()
        {
            if (dataGridObject.SelectedCells.Count == 0)
                return;

            selected = dataGridObject.SelectedCells[0].Item as Table.PayS;
        }

        private void useInvoice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (selected != null)
                selected.UseInvoice = ((ComboBox)sender).SelectedItem.ToString().Contains("是");
        }

        private void Save()
        {
            foreach (Table.PayS p in payS)
            {
                if (!p._Pay)
                    continue;

                p.OrderDateTime = DateTime.Now;
                p.BankId = uCUniqueSBank.SelectedObjectId;
                p.Voucher = textBoxVoucher.Text;
                Table.DataTableS.EnumDatabaseStatus result = p.ShenHe(checkBoxCorSMoney.IsChecked.Value);
                if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                    MessageWindow.Show(result.ToString());
            }
            this.Close();
        }

        private void ckbSelectedAll_Checked(object sender, RoutedEventArgs e)
        {
            if (label1Due.Content == labelMoney.Content)
                return;

            selectMoney = (double)label1Due.Content;
            labelMoney.Content = selectMoney;
            foreach (Table.PayS p in payS)
                p._Pay = true;
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            DataGridTextColumn textColumn = ee.Column as DataGridTextColumn;
            if (textColumn == null)
                return;

            string binding = ((Binding)textColumn.Binding).Path.Path;
            double d = 0;
            Table.PayS pay = ee.Row.Item as Table.PayS;
            Debug.Assert(pay != null);

            switch (binding)
            {
                case "RealMoney":
                    TextBox textBox = (TextBox)ee.EditingElement;
                    if (!double.TryParse(textBox.Text, out d) || d < 0||pay.Advance)
                        textBox.Text = pay.RealMoney.ToString();
                    
                    break;
            }
        }
    }
}