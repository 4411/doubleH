﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility;

namespace PayS
{
    /// <summary>
    /// ShouKuan.xaml 的交互逻辑
    /// </summary>
    public partial class OtherBill : Window
    {
        public OtherBill()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.PayS order = null;
        public void Init(DataTableText tableText, Table.PayS obj)
        {
            order = obj ?? new Table.PayS(tableText == DataTableText.其它费用收入单 ? Table.PayS.EnumFlag.其它费用收入单 : Table.PayS.EnumFlag.其它费用支出单);

            //order.OrderDateTime = DateTime.Now;

            kuaiJiKeMu.ItemsSource = Table.UniqueS.GetListForKeMu(false, (tableText == DataTableText.其它费用收入单 ? Table.UniqueS.EnumKeMuType.收入类 : Table.UniqueS.EnumKeMuType.支出类));
            kuaiJiKeMu.SelectedValuePath = "StringValue";
            kuaiJiKeMu.SelectedValueBinding = new Binding("KeMu");
            kuaiJiKeMu.DisplayMemberPath = "_Name";

            uCUniqueSBank.Init(Table.UniqueS.EnumFlag.支付方式);
            label1.Content = this.Title = tableText.ToString();
            dataGridObject.SelectedValuePath = "Money";
            InitOrder();
        }

        private void InitOrder()
        {
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            labelOrderNO.Content = order.OrderNO;
            textBoxNote.Text = order.Note;
            dataGridObject.ItemsSource = order._PayDetail;
            uCUniqueSBank.SelectedObjectId = order.BankId;

            if (order.Enable == Table.PayS.EnumEnable.审核)
            {
                buttonOK.IsEnabled = false;
                dataGridObject.IsReadOnly = true;
            }
        }

        private void InitEvent()
        {
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            buttonOK.Click += (ss, ee) => Save();
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            DataGridColumn textColumn = ee.Column as DataGridColumn;
            double d = 0;
            switch (textColumn.Header.ToString())
            {
                case "金额":
                    TextBox textBox = (TextBox)ee.EditingElement;
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d < 0)
                            textBox.Undo();

                    break;
            }
        }

        private void Save()
        {
            order.OrderDateTime = dateTimeUpDownKaiDan.Value;
            order.Note = textBoxNote.Text;
            order.BankId = uCUniqueSBank.SelectedObjectId;
            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        //private void SetMoney()
        //{
        //    if (dataGridObject.SelectedItem == null)
        //        return;

        //    Table.PayS ps = (Table.PayS)dataGridObject.SelectedItem;
        //    var cntr = dataGridObject.ItemContainerGenerator.ContainerFromItem(dataGridObject.SelectedItem);
        //    DataGridRow ObjROw = (DataGridRow)cntr;
        //    if (ObjROw != null)
        //    {
        //        FrameworkElement objElement = dataGridObject.Columns[0].GetCellContent(ObjROw);
        //        if (objElement != null)
        //        {
        //            System.Windows.Controls.CheckBox objChk = (System.Windows.Controls.CheckBox)objElement;
        //            if (objChk.IsChecked != true)
        //                selectMoney += ps.Money;
        //            else
        //                selectMoney -= ps.Money;
        //        }
        //    }
        //    labelMoney.Content = selectMoney;
        //}
    }
}