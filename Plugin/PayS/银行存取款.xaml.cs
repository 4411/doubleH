﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;
using System.Diagnostics;
using DoubleH.Utility;

namespace PayS
{
    /// <summary>
    /// BankMoney.xaml 的交互逻辑
    /// </summary>
    public partial class BankMoney : Window
    {
        public BankMoney()
        {
            InitializeComponent();
        }

        Table.BankMoneyS order = null;
        public void Init(Table.BankMoneyS obj)
        {
            order = obj ?? new Table.BankMoneyS();

            InitVar();
            InitOrder();
            InitEvent();
        }

        ObservableCollection<Table.UniqueS> allBank;
        private void InitVar()
        {
            allBank = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.支付方式, Table.UniqueS.EnumEnable.启用);

            bankIdOut.DisplayMemberPath = "Name";
            bankIdOut.SelectedValuePath = "Id";
            bankIdOut.SelectedValueBinding = new Binding("BankIdOut");
            bankIdOut.ItemsSource = allBank;

            bankIdIn.DisplayMemberPath = "Name";
            bankIdIn.SelectedValuePath = "Id";
            bankIdIn.SelectedValueBinding = new Binding("BankIdIn");
            bankIdIn.ItemsSource = allBank;
        }

        private void InitOrder()
        {
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            labelOrderNO.Content = order.OrderNO;
            textBoxNote.Text = order.Note;
            dataGridObject.ItemsSource = order._DetailText;
            buttonOK.IsEnabled = order.Id == -1;
            dataGridObject.IsReadOnly = !buttonOK.IsEnabled;
        }

        private void InitEvent()
        {
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            buttonOK.Click += (ss, ee) => Save();
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            DataGridColumn textColumn = ee.Column as DataGridColumn;
            double d = 0;
            switch (textColumn.Header.ToString())
            {
                case "金额":
                    TextBox textBox = (TextBox)ee.EditingElement;
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d < 0)
                            textBox.Undo();

                    break;
            }
        }

        private void Save()
        {
            Debug.Assert(order.Id == -1);

            order.OrderDateTime = dateTimeUpDownKaiDan.Value.Value;
            order.Note = textBoxNote.Text;
            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}