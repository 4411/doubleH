﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace CarS.UC
{
    /// <summary>
    /// UCuserSelect.xaml 的交互逻辑
    /// </summary>
    public partial class UCCarSelect: ComboBox
    {
        public UCCarSelect()
        {
            InitializeComponent();

            this.DisplayMemberPath = "Name";
            this.SelectedValuePath = "Id";
        }

        public void Init(params Table.CarS.EnumEnable[] enable)
        {
            this.ItemsSource = Table.CarS.GetList(enable);
        }

        public Table.CarS SelectedObject
        {
            get
            {
                if (SelectedItem == null)
                    return null;
                else
                    return (Table.CarS)SelectedItem;
            }
            set
            {
                SelectedItem = value;
            }
        }

        public Int64 SelectedObjectId
        {
            get
            {
                if (SelectedObject == null)
                    return -1;
                else
                    return SelectedObject.Id;
            }
            set { SelectedValue = value; }
        }
    }
}
