﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CarS
{
    /// <summary>
    /// Oil.xaml 的交互逻辑
    /// </summary>
    public partial class OilWindow : Window
    {
        public OilWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.CarOilS orderS = null;
        public void Init(Table.CarOilS oil)
        {
            orderS = oil ?? new Table.CarOilS();

            uCCarSelect1.SelectedObjectId = orderS.CarSId;
            corSelect1.SelectedObjectId = orderS.CorSId;
            userSelect1.SelectedObjectId = orderS.OperatorerSId;
            textBoxNote.Text = orderS.Note;
            dateTimePickerDate.Value = orderS.OrderDateTime;
            textBoxBillNO.Text = orderS.OrderNO;
            doubleUpDownMoney.Value = orderS.Money;

            buttonShenHe.IsEnabled = (orderS.Id != -1 && orderS.Enable == Table.CarOilS.EnumEnable.评估);
            buttonSave.IsEnabled = orderS.Enable == Table.CarOilS.EnumEnable.评估;
        }

        private void InitVar()
        {
            doubleUpDownMoney.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();

            uCCarSelect1.Init(Table.CarS.EnumEnable.待用, Table.CarS.EnumEnable.年审, Table.CarS.EnumEnable.维修, Table.CarS.EnumEnable.行车);
            corSelect1.Init(true);
            userSelect1.Init(Table.UserS.EnumFlag.经办人);
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            buttonShenHe.Click += (ss, ee) =>
            {
                orderS.UpdateTo(Table.CarOilS.EnumEnable.入账);
                this.Close();
            };
        }

        private void Save()
        {
            orderS.CarSId = uCCarSelect1.SelectedObjectId;
            orderS.CorSId = corSelect1.SelectedObjectId;
            orderS.OperatorerSId = userSelect1.SelectedObjectId;
            orderS.Note = textBoxNote.Text;
            orderS.OrderDateTime = dateTimePickerDate.Value.Value;
            orderS.OrderNO = textBoxBillNO.Text;
            orderS.Money = doubleUpDownMoney.Value.Value;

            orderS.UserSId = Table.UserS.LoginUser.Id;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (orderS.Id == -1)
                result = orderS.Insert();
            else
                result = orderS.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}
