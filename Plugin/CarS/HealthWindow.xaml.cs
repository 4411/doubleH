﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CarS
{
    /// <summary>
    /// HealthWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HealthWindow : Window
    {
        public HealthWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.CarHealthS order = null;
        public void Init(Table.CarHealthS obj)
        {
            order = obj;
            if (order == null)
            {
                buttonSave.Visibility = Visibility.Hidden;
                buttonShenHe.Visibility = Visibility.Hidden;
                return;
            }

            if (order.Enable == Table.CarHealthS.EnumEnable.处理完毕)
            {
                buttonSave.Visibility = Visibility.Hidden;
                buttonShenHe.Visibility = Visibility.Hidden;
            }
            else
            {
                if (order.Id == -1)
                {
                    dateTimePickerEnd.IsReadOnly = true;
                    buttonShenHe.Visibility = Visibility.Hidden;
                }
                else
                    buttonSave.Content = "更新";
            }

            uCCarSelect1.SelectedObjectId = order.CarSId;
            userSelectOperator.SelectedObjectId = order.OperatorerSId;
            corSelect1.SelectedObjectId = order.CorSId;

            textBoxRelatedOrderNO.Text = order.OrderNO;
            textBoxNote.Text = order.Note;
            dateTimePickerStart.Value = order.DateStart;
            dateTimePickerEnd.Value = order.DateEnd;
            doubleUpDownMoney.Value = order.Money;
        }

        private void InitVar()
        {
            doubleUpDownMoney.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();
            corSelect1.Init(false);
            uCCarSelect1.Init(Table.CarS.EnumEnable.待用, Table.CarS.EnumEnable.年审, Table.CarS.EnumEnable.维修, Table.CarS.EnumEnable.行车);
            userSelectOperator.Init(Table.UserS.EnumFlag.操作员,Table.UserS.EnumFlag.经办人);
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) =>  Save(); 
            buttonShenHe.Click += (ss, ee) => ShenHe();
        }

        private void Save()
        {
            order.CarSId = uCCarSelect1.SelectedObjectId;
            order.OperatorerSId = userSelectOperator.SelectedObjectId;
            order.Note = textBoxNote.Text;
            order.DateStart = dateTimePickerStart.Value.Value;
            order.Money = doubleUpDownMoney.Value.HasValue ? doubleUpDownMoney.Value.Value : 0;
            order.CorSId = corSelect1.SelectedObjectId;
            order.OrderNO = textBoxRelatedOrderNO.Text;
            order.UserSId = Table.UserS.LoginUser.Id;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void ShenHe()
        {
            order.DateEnd = dateTimePickerEnd.Value.HasValue ? dateTimePickerEnd.Value.Value : DateTime.Now;
            order.OrderNO = textBoxRelatedOrderNO.Text;
            doubleUpDownMoney.Value = order.Money;
            order.UpdateTo(Table.CarHealthS.EnumEnable.处理完毕);
            this.Close();
        }
    }
}
