﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace UniqueS
{
    /// <summary>
    /// WindowVip.xaml 的交互逻辑
    /// </summary>
    public partial class WindowVip : Window
    {
        public WindowVip()
        {
            InitializeComponent();
        }

        Table.UniqueS orderS = null;

        public void Init(Table.UniqueS.EnumFlag flag, Table.UniqueS tag)
        {
            this.Title = flag.ToString();
            orderS = tag ?? new Table.UniqueS(flag);

            comboBoxType.ItemsSource = Enum.GetNames(typeof(Table.UniqueS.EnumVipType));
            comboBoxType.SelectedIndex = orderS.IntegerValue == -1 ? 0 : (int)orderS.IntegerValue;

            textBoxName.Text = orderS.Name;
            textBoxNote.Text = orderS.Note;
            doubleUpDownDoubleValue.Value = orderS.DoubleValue==0?1:orderS.DoubleValue;
            checkBoxScore.IsChecked = orderS.BoolValue;
            textBoxName.Focus();

            InitEvent();
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => { Save(); };
            comboBoxType.SelectionChanged += (ss, ee) => { doubleUpDownDoubleValue.IsEnabled = (comboBoxType.SelectedIndex==(int)Table.UniqueS.EnumVipType.折扣); };
        }

        private bool ControlsValidating()
        {
            bool b = false;
            if (string.IsNullOrWhiteSpace(textBoxName.Text))
            {
                MessageWindow.Show("名称不能为空");
                textBoxName.Focus();
            }
            else
                b = true;

            return b;
        }

        private void Save()
        {
            if (!ControlsValidating())
                return;

            orderS.Name = textBoxName.Text;
            orderS.Note = textBoxNote.Text;
            orderS.BoolValue = checkBoxScore.IsChecked == true;
            orderS.IntegerValue = comboBoxType.SelectedIndex;
            orderS.DoubleValue = doubleUpDownDoubleValue.Value.Value;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (orderS.Id == -1)
                result = orderS.Insert();
            else
                result = orderS.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
            {
                MessageWindow.Show(result.ToString());
                textBoxName.Focus();
            }
        }
    }
}
