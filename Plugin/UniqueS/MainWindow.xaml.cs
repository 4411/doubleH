﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using DoubleH.Utility;
using DoubleH;

namespace UniqueS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.UniqueS order = null;
        public void Init(Table.UniqueS.EnumFlag flag, Table.UniqueS tag)
        {
            this.Title = flag.ToString();
            order = tag ?? new Table.UniqueS(flag);

            comboBoxName.Text = order.Name;
            textBoxNote.Text = order.Note;
            doubleUpDownDoubleValue.Value = order.DoubleValue;
            textBoxStringValue.Text = order.StringValue;
            checkBox1.IsChecked = order.Enable == Table.UniqueS.EnumEnable.停用 ? true : false;

            canvasDoubleValue.Visibility = Visibility.Collapsed;
            canvasStringValue.Visibility = Visibility.Collapsed;
            this.Height -= canvasDoubleValue.Height;
            switch (order.Flag)
            {
                case Table.UniqueS.EnumFlag.会计科目:
                     canvasStringValue.Visibility = Visibility.Visible;
                     labelStringValue.Content = "科目代码:";
                    this.Height += canvasStringValue.Height;
                    if (order.Id != -1&&order.BoolValue)
                            this.Title += " - 系统预置字段（禁止编辑）";
                   
                    break;

                case Table.UniqueS.EnumFlag.支付方式:
                     canvasStringValue.Visibility = Visibility.Visible;
                     labelStringValue.Content = "账号:";
                    this.Height += canvasStringValue.Height;
                    break;

                case Table.UniqueS.EnumFlag.服务级别定义:
                    labelDouble.Content = "响应:";
                    canvasDoubleValue.Visibility = Visibility.Visible;
                    this.Height += canvasDoubleValue.Height;
                    break;

                case Table.UniqueS.EnumFlag.区域:
                    labelTime.Visibility = Visibility.Hidden;
                    canvasDoubleValue.Visibility = Visibility.Visible;
                    this.Height += canvasDoubleValue.Height;
                    break;

                case Table.UniqueS.EnumFlag.发票定义:
                    labelTime.Visibility = Visibility.Hidden;
                    labelDouble.Content = "税率:";
                    doubleUpDownDoubleValue.Minimum = 0.01;
                    doubleUpDownDoubleValue.Maximum = 0.5;
                    doubleUpDownDoubleValue.Increment = 0.01;
                    canvasDoubleValue.Visibility = Visibility.Visible;
                    this.Height += canvasDoubleValue.Height;
                    break;
            }

            comboBoxName.Focus();
            InitEvent();
        }

        private void InitEvent()
        {
            buttonOk.Click += (ss, ee) =>  Save();
        }

        private void Save()
        {
            order.Name = comboBoxName.Text;
            order.Note = textBoxNote.Text;
            order.DoubleValue = doubleUpDownDoubleValue.Value.Value;
            order.StringValue = textBoxStringValue.Text;
            order.Enable = checkBox1.IsChecked == true ? Table.UniqueS.EnumEnable.停用 : Table.UniqueS.EnumEnable.启用;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
            {
                MessageWindow.Show(result.ToString());
                comboBoxName.Focus();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
