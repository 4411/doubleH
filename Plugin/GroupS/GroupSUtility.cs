﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupS
{
   public class GroupSUtility
    {
        public class GroupSBinding
        {
            public Int64 Id { get; set; }
            public string Name { get; set; }
            public Int64 ParentId { get; set; }

            public List<GroupSBinding> GroupSBindings
            {
                get;
                set;
            }
        }
    }
}
