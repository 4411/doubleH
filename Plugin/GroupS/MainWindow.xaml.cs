﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;
using System.Data;
using DoubleH.Utility.IO;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace GroupS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.GroupS order = null;
        string imageFile = string.Empty;//只有点击按钮选择图片之后,才赋值给它,防止重复的更新图片文件到服务器
        bool sendImageFile = false;

        public Table.GroupS NewGroupS
        {
            get { return order; }
        }
        List<Table.GroupS> gs;

        public void Init(Table.GroupS.EnumFlag flag, Table.GroupS group)
        {
            order = group ?? new Table.GroupS(flag);
            gs = new List<Table.GroupS>(Table.GroupS.GetList(flag, Table.GroupS.EnumEnable.启用));
            gs.Remove(gs.Find(f => { return f.Id == order.Id; }));
            gs.Insert(0, new Table.GroupS(flag) { Name = "顶层分类" });//添加父类

            if (flag == Table.GroupS.EnumFlag.操作员分类)
            {
                comboBoxGroup.IsEnabled = false;
                checkBox1.IsEnabled = false;
            }
            else
            {
                comboBoxGroup.ItemsSource = gs;
                comboBoxGroup.DisplayMemberPath = "Name";
                comboBoxGroup.SelectedValuePath = "Id";
                comboBoxGroup.SelectedValue = order.ParentId;
                checkBox1.IsChecked = order.Enable == Table.GroupS.EnumEnable.停用 ? true : false;
            }

            textBoxName.Text = order.Name;
            buttonHeBin.IsEnabled = order.Id != -1;
            textBoxName.Focus();
            //图片
            if (order.Id != -1 && !string.IsNullOrEmpty(order.UploadFile))
            {
                string path = TransferFiles.GetFile(new FileFormat() { TableName = "GroupS", FileName = order.UploadFile, FileFlag = FileType.请求文件 });
                if (!string.IsNullOrEmpty(path))
                    image1.Source = new BitmapImage(new Uri(path));
            }
        }

        private void InitEvent()
        {
            buttonOk.Click += (ss, ee) =>  Save(); 
            buttonHeBin.Click += (ss, ee) =>  HeBing();
            buttonImage.Click += (ss, ee) => SelectImg();
            image1.MouseLeftButtonUp += (ss, ee) => RemoveImg(ee);
        }

        private void RemoveImg(MouseButtonEventArgs ee)
        {
                image1.Source = null;
                sendImageFile = false;
        }

        private void SelectImg()
        {
            imageFile = FileTools.GetFile(true, ".jpg", ".bmp", ".png", ".gif");
            if (!string.IsNullOrEmpty(imageFile))
            {
                image1.Source = new BitmapImage(new Uri(imageFile));
                sendImageFile = true;
            }
        }

        private void HeBing()
        {
            if (comboBoxGroup.SelectedItem == null)
                return;

            if (MessageWindow.Show("", "确定要合并类别吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            Table.DataTableS.EnumDatabaseStatus result = order.HeBing(((Table.GroupS)comboBoxGroup.SelectedItem).Id);
            MessageWindow.Show(result.ToString());
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                buttonHeBin.IsEnabled = false;
        }

        private void Save()
        {
            order.Name = textBoxName.Text.Trim();
            order.Enable = checkBox1.IsChecked == true ? Table.GroupS.EnumEnable.停用 : Table.GroupS.EnumEnable.启用;
            order.ParentId = comboBoxGroup.SelectedItem == null ? -1 : (Int64)comboBoxGroup.SelectedValue;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
            {
                if (string.IsNullOrEmpty(imageFile))
                    order.UploadFile = string.Empty;
                else
                    order.UploadFile = order.Id.ToString() + "_" + DateTime.Now.ToString(DbDefine.dateTimeFormat) + imageFile.Substring(imageFile.LastIndexOf('.'));

                order.UpdateImage();
                if (!string.IsNullOrEmpty(imageFile) && sendImageFile)
                {
                    FileFormat ff = new FileFormat()
                    {
                        FileFlag = FileType.传输文件,
                        IP = DoubleHConfig.AppConfig.ServerIP,
                        Port = DoubleHConfig.AppConfig.ServerPort.ToString(),
                        FilePath = imageFile,
                        TableName = "GroupS",
                        FileName = order.UploadFile
                    };

                    if (!TransferFiles.SaveFile(ff))
                        MessageWindow.Show("图片文件上传失败");
                }

                this.Close();
            }
            else
                MessageWindow.Show(result.ToString());
        }
    }
}
