﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using Table = FCNS.Data.Table;

namespace StoreS
{
    /// <summary>
    /// KuCunTiaoJia.xaml 的交互逻辑
    /// </summary>
    public partial class KuCunTiaoJia : Window
    {
        public KuCunTiaoJia()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.AdjustAveragePriceS order = null;
        public void Init(Table.AdjustAveragePriceS obj)
        {
            order = obj ?? new Table.AdjustAveragePriceS();
         
            dataGridObject.ShowBottom = false;
            dataGridObject.ProductSType = ProductS.GetProductS.EnumProductS.库存调价单商品;
            dataGridObject.Init(DoubleHConfig.UserConfig.GetUserUIparams(DataTableText.库存调价单商品编辑));
            dataGridObject.CanNegativeSales = true;

            InitOrder();

            dataGridObject.StoreS = uCStoreS1.SelectedObject;
        }

        private void InitOrder()
        {
            label1OrderNo.Content = order.OrderNO;
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            textBoxNote.Text = order.Note;
            dataGridObject.ItemsSource = order.ProductSList;
            if (order.Id == -1)
                return;

            uCStoreS1.SelectedObjectId = order.StoreSId;
            dataGridObject.IsReadOnly = true;
            buttonSave.IsEnabled = false;
            uCStoreS1.IsEnabled = false;
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            uCStoreS1.SelectedObjectEvent += (ss) => ChangeStoreSId(ss);
        }

        private void ChangeStoreSId(Table.StoreS ss)
        {
            order.ProductSList.Clear();
            dataGridObject.StoreS = ss; 
        }

        private void Save()
        {
            System.Diagnostics.Debug.Assert(order.Id == -1);

            order.Note = textBoxNote.Text;
            order.OrderDateTime = dateTimeUpDownKaiDan.Value.Value;
            order.StoreSId = uCStoreS1.SelectedObjectId;
            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}