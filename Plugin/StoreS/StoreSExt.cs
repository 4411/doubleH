﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using System.Windows;
using DoubleH.Utility;
using DoubleH;

namespace StoreS
{
    public class StoreSExt : Plugin
    {
        MainWindow mainWin = null;
        StoreSIO ioWin = null;
        DiaoBo diaoBo = null;
        KuCunTiaoJia kuCun = null;
        PanDian panDian = null;
        IPluginHost ihost = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            ihost = host;
            switch (TableText)
            {
                case DataTableText.非进货入库单:
                case DataTableText.非销售出库单:
                    ioWin = new StoreSIO(host);
                    Table.StoreOrderS s1 = DataTable as Table.StoreOrderS;
                    ioWin.Init(s1, TableText);
                    ioWin.Owner = host.WorkAreaWindow;
                    ioWin.Show();
                    //ioWin.Closed += (ss, ee) => { ihost.WorkAreaWindow.RefreshDataGrid(); };
                    break;

                default:
                    switch (TableText)
                    {
                        case DataTableText.盘点单:
                            panDian = new PanDian();
                            panDian.Init(DataTable as Table.CheckStoreS);
                            panDian.ShowDialog();
                            break;

                        case DataTableText.调拨单:
                            diaoBo = new DiaoBo();
                            diaoBo.Init(DataTable as Table.AllocationS);
                            diaoBo.ShowDialog();
                            break;

                        case DataTableText.库存调价单:
                            kuCun = new KuCunTiaoJia();
                            kuCun.Init(DataTable as Table.AdjustAveragePriceS);
                            kuCun.ShowDialog();
                            break;

                        case DataTableText.仓库:
                            mainWin = new MainWindow();
                            mainWin.Init(DataTable as Table.StoreS);
                            mainWin.ShowDialog();
                            break;

                        default:
                            ioWin = new StoreSIO(host);
                            Table.StoreOrderS s2 = DataTable as Table.StoreOrderS;
                            if (s2 == null)//如果是通过点击"商品出入库"的就在下面验证
                            {
                                Table.ProductSIO product = (DataTable as Table.ProductSIO);
                                if (product != null)
                                {
                                    if (product.OrderNO.StartsWith("CG") || product.OrderNO.StartsWith("PF"))
                                        s2 = Table.StoreOrderS.GetObjectByRelatedOrderNO(product.OrderNO);
                                    else
                                        s2 = Table.StoreOrderS.GetObject(product.OrderNO);
                                }
                            }

                            if (s2 == null)
                            {
                                Terminate();
                                return false;
                            }
                            ioWin.Init(s2, TableText);
                            ioWin.Owner = host.WorkAreaWindow;
                            ioWin.Show();
                            break;
                    }

                    //Terminate();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (mainWin != null)
                mainWin.Close();

            if (ioWin != null)
                ioWin.Close();

            if (panDian != null)
                panDian.Close();

            if (kuCun != null)
                kuCun.Close();

            if (diaoBo != null)
                diaoBo.Close();
        }
    }
}