﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FCNS.Data.Table;
using System.Diagnostics;
using BrightIdeasSoftware;
using DoubleH.Utility;
using DoubleH.Plugins;
using Table=FCNS.Data.Table;

namespace StoreS
{
    public partial class MainIO : Form
    {
        public MainIO()
        {
            InitializeComponent();
        }

        Table.StoreOrderS orderS = null;
        public virtual void Init(Table.StoreOrderS order)
        {
            if (DesignMode)
                return;

            Debug.Assert(order != null);
            orderS = order;
            InitControls();
        }

        private void InitControls()
        {
            Debug.Assert(orderS != null);
            if (orderS.Enable == StoreOrderS.EnumEnable.评估)
            {
                buttonZuoFei.Enabled = true;
                buttonShenHe.Enabled = true;
            }
            else
            {
                buttonZuoFei.Enabled = false;
                buttonShenHe.Enabled = false;
            }

            labelDateTimeValue.Text = orderS.OrderDateTime.ToString();
            labelOrderNO.Text = orderS.OrderNo;
            textBoxNoteNo.Text = orderS.NoteNo;
            labelRelatedNoValue.Text = orderS.RelatedOrderNo;
            textBoxNote.Text = orderS.Note;
            labelStoreValue.Text = orderS.StoreSIdText;
            //是采购单还是批发单
            if (orderS.RelatedOrderNo.StartsWith("CG"))
            {
                PurchaseOrderS pos = PurchaseOrderS.GetObject(orderS.RelatedOrderNo);
                labelSupplierValue.Text = pos.SupplierSName;
                labelAddressValue.Text = pos.Address;
            }
            else
            {
                SalesOrderS sos = SalesOrderS.GetObject(orderS.RelatedOrderNo);
                labelSupplierValue.Text = sos.CustomerSName;
                labelAddressValue.Text = sos.Address;
            }
            objectListView1.SetObjects(orderS.ProductSList);

            SetOverLay();
        }

        TextOverlay nagOverlay;
        private void SetOverLay()
        {
            objectListView1.RemoveOverlay(this.nagOverlay);
            if (objectListView1.Items.Count == 0)
                return;

            this.nagOverlay = new TextOverlay();
            this.nagOverlay.Alignment = ContentAlignment.BottomRight;
            this.nagOverlay.Text = orderS.EnableText;
            this.nagOverlay.BackColor = Color.White;
            this.nagOverlay.BorderWidth = 2.0f;
            this.nagOverlay.BorderColor = Color.RoyalBlue;
            this.nagOverlay.TextColor = Color.Red;
            objectListView1.OverlayTransparency = 255;

            objectListView1.AddOverlay(this.nagOverlay);
        }

        private void buttonPre_Click(object sender, EventArgs e)
        {
            Table.StoreOrderS p = Table.StoreOrderS.GetPreObject(orderS.Id);
            if (p == null)
                MessageWindow.Show("没有上一条记录了");
            else
            {
                orderS = p;
                Init(orderS);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            Table.StoreOrderS p = Table.StoreOrderS.GetNextObject(orderS.Id);
            if (p == null)
                MessageWindow.Show("没有下一条记录了");
            else
            {
                orderS = p;
                Init(orderS);
            }
        }

        private void buttonZuoFei_Click(object sender, EventArgs e)
        {
            if (orderS.Enable == StoreOrderS.EnumEnable.评估)
                if (System.Windows.Forms.MessageBox.Show("确定要作废此单据吗?", "", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;

            orderS.ZuoFei();
            MessageWindow.Show("单据已作废,可以取消相关的采购单/批发单");
        }

        private void buttonShenHe_Click(object sender, EventArgs e)
        {
            orderS.UserId = UserS.LoginUser.Id;
            orderS.NoteNo = textBoxNoteNo.Text.Trim();
            orderS.Note = textBoxNote.Text.Trim();
            orderS.ShenHe();
            SetOverLay();
            buttonShenHe.Enabled = false;
            buttonZuoFei.Enabled = false;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Print.PrintPage(orderS);
        }
    }
}