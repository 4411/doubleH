﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace StoreS.UC
{
  public  class UCStoreS:ComboBox
    {
        public UCStoreS()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        public delegate void Selected(Table.StoreS obj);
        public event Selected SelectedObjectEvent;

        public Table.StoreS SelectedObject
        {
            get
            {
                if (this.SelectedItem == null)
                    return null;
                else
                    return (Table.StoreS)this.SelectedItem;
            }
            set { this.SelectedItem = value; }
        }

        public Int64 SelectedObjectId
        {
            get { return SelectedObject == null ? -1 : SelectedObject.Id; }
            set { this.SelectedValue = value; }
        }

        private void InitVar()
        {
           this.DisplayMemberPath = "Name";
            this.SelectedValuePath = "Id";
            this.ItemsSource = Table.StoreS.GetList(Table.StoreS.EnumEnable.启用);
            if (this.HasItems)
                this.SelectedIndex = 0;
        }

        private void InitEvent()
        {
            this.SelectionChanged += (ss, ee) =>
            {
                if (SelectedObjectEvent != null && this.SelectedItem != null)
                    SelectedObjectEvent((Table.StoreS)this.SelectedItem);
            };
        }
    }
}
