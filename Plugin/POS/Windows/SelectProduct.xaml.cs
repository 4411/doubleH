﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;

namespace POS
{
    /// <summary>
    /// WindowProduct.xaml 的交互逻辑
    /// </summary>
    public partial class SelectProduct : Window
    {
        public SelectProduct()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.ProductS ps = null;
        /// <summary>
        /// 已选择的商品
        /// </summary>
        public Table.ProductS Product
        {
            get { return ps; }
        }

        public System.Collections.IEnumerable ItemsSource
        {
            get { return productSListInfo1.ItemsSource; }
            set
            {
                productSListInfo1.ItemsSource = value;
                if (!productSListInfo1.HasItems)
                    return;

                    productSListInfo1.SelectedIndex = 0;
                    this.Height = 75 + productSListInfo1.Items.Count * 35;
            }
        }

        private void InitVar()
        {
            productSListInfo1.ShowBodyMenu = false;
            productSListInfo1.ShowHeaderMenu = false;
        }
    
        private void InitEvent()
        {
            this.KeyDown += (ss, ee) => { KeyDowned(ee.Key); };
            productSListInfo1.MouseDoubleClick += (ss, ee) => { GetProduct(); };
            productSListInfo1.KeyDown += (ss, ee) => { KeyDowned(ee.Key); };
        }

        private void KeyDowned(Key key)
        {
            switch (key)
            {
                case Key.Enter:
                    GetProduct();
                    break;

                case Key.Down:
                    if (productSListInfo1.SelectedIndex + 1 < productSListInfo1.Items.Count)
                        productSListInfo1.SelectedIndex += 1;
                    break;

                case Key.Up:
                    if (productSListInfo1.SelectedIndex > 0)
                        productSListInfo1.SelectedIndex -= 1;
                    break;

                case Key.Escape:
                    this.Close();
                    break;
            }
        }

        private void GetProduct()
        {
            if (productSListInfo1.SelectedItem == null)
                return;

            ps = (Table.ProductS)productSListInfo1.SelectedItem;
            this.Close();
        }
    }
}
