﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Table=FCNS.Data.Table;

namespace POS
{
    public enum EnumEditProduct
    {
        添加,
        移除,
        编辑数量,
        编辑价格
    }

    //模板共用的相同的事情用事件实现
    public delegate void dNoParams();
    /// <summary>
    /// 
    /// </summary>
    /// <param name="eep">如果是‘添加’状态，当商品列表中已存在有此商品时，数量自动+1</param>
    /// <param name="product"></param>
    /// <param name="quantity">新的数量</param>
    /// <param name="price">新的价格</param>
    public delegate void dEditProduct(EnumEditProduct eep, Table.ProductS product, double quantity, double price);
    public delegate void dLoginToSaleEvent(Table.UserS user, EnumMode mode);
    public delegate void dPayToSaleEvent(double payMoney, bool isPayed);
    public delegate void dSaleToPayEvent();
    public delegate void dZengSong();
    /// <summary>
    /// 挂单
    /// </summary>
    /// <param name="isTemp">true 临时挂单，注销用户就会被清空，false 写入数据库，在财务模块结算。</param>
    /// <param name="addOrRemove">是添加到挂单列表还是取出</param>
    /// <param name="flag">挂单列表中单据的标识，临时单：客商Id，永久单：单号</param>
    public delegate void dGuaDan(bool isTemp, bool addOrRemove, string flag);

    /// <summary>
    /// 登陆页面
    /// </summary>
    public interface ILoginTemplete
    {
        event dLoginToSaleEvent Logined;
    }
    /// <summary>
    /// 开单页面
    /// </summary>
    public interface ISaleTemplete
    {
        event dSaleToPayEvent Saled;
        /// <summary>
        /// 赠送  接口
        /// 自动识别是开始还是结束
        /// </summary>
        event dZengSong ZengSong;
        event dEditProduct EditProduct;
        event dNoParams JiaoBan;
        event dGuaDan GuaDan;
        /// <summary>
        /// 切换零售还是退货模式,如果退货模式结单后自动转换成零售模式.
        /// </summary>
        event dNoParams TuiHuo;

        void Init();
        /// <summary>
        /// 读取App.Order到控件
        /// </summary>
        void InitOrder();
        /// <summary>
        /// 当一张订单完成后,只要用于 SaleTemplete 的界面更新
        /// </summary>
        /// <param name="order"></param>
        void OrderFinish(Table.PosOrderS order);
        /// <summary>
        /// 当挂单/取单完成后,只要用于 SaleTemplete 的界面更新
        /// </summary>
        void GuaDanFinish(System.Collections.IList guaDanList);

        FCNS.Data.Table.StoreS StoreS { set; }
    }

    /// <summary>
    /// 支付页面
    /// </summary>
    public interface IPayTemplete
    {
        event dPayToSaleEvent Payed;
    }
}