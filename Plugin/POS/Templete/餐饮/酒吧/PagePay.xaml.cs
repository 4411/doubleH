﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.IO;
using DoubleH.Utility.Configuration;

namespace POS.Templete.BarTemplete
{
    /// <summary>
    /// PagePay.xaml 的交互逻辑
    /// </summary>
    public partial class PagePay : Page, IPayTemplete
    {
        public PagePay()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        public event dPayToSaleEvent Payed;
        DoubleH.Utility.IO.PosLed posLed = null;

        private void InitVar()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as Control;
                if (c != null)
                {
                    if (c.Name == "buttonCanel" || c.Name == "buttonPay")
                        continue;

                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }
            if (!string.IsNullOrEmpty(DoubleHConfig.AppConfig.PosLed) && DoubleHConfig.AppConfig.PosLed != "无")
                posLed = new DoubleH.Utility.IO.PosLed();
        }

        public void InitEvent()
        {
            doubleUpDownPay.ValueChanged += (ss, ee) => ChangeMoney();
            buttonCanel.Click += (ss, ee) => PayCanel();
            buttonPay.Click += (ss, ee) => Pay();
            this.Loaded += (ss, ee) => ThisOnLoad();
        }

        private void ThisOnLoad()
        {
            labelSum.Content = App.Order.Money;
            doubleUpDownPay.Value = App.Order.Money;
            doubleUpDownPay.Focus();
            if (posLed != null)
            {
                posLed.Open();
                posLed.Sum(App.Order.Money.ToString());
            }
        }

        private void ChangeMoney()
        {
            double d = doubleUpDownPay.Value.HasValue ? doubleUpDownPay.Value.Value : 0;
            d = d - App.Order.Money;
            labelBack.Content = (d > 0 ? d : 0);
        }

        public void PayCanel()
        {
            if (Payed != null)
                Payed(0, false);
        }

        public void Pay()
        {
            if (doubleUpDownPay.Value.HasValue)
            {
                if (doubleUpDownPay.Value.Value < App.Order.Money)
                {
                    MessageWindow.Show("注意", "收款金额低于账单金额.");
                    return;
                }

                if (Payed == null)
                    return;

                if (posLed != null)
                {
                    posLed.Get(doubleUpDownPay.Value.ToString());
                    posLed.OddChange(labelBack.Content.ToString());
                    posLed.Close();
                }
                Payed(doubleUpDownPay.Value.Value, true);
            }
            else
                MessageWindow.Show("请输入收款金额");
        }
    }
}