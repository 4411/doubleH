﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace MessageS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.ScheduleS order = null;
        public void Init(Table.ScheduleS obj)
        {
            order = obj ?? new Table.ScheduleS();
            InitOrder();
            textBoxPlannedTasks.Focus();
        }

        private void InitVar()
        {
            comboBoxFlag.ItemsSource = Enum.GetNames(typeof(Table.ScheduleS.EnumFlag));
        }

        private void InitEvent()
        {
            buttonUpdate.Click += (ss, ee) => UpdateOrder();
            buttonZuoFei.Click += (ss, ee) => ZuoFeiOrder();
            buttonSave.Click += (ss, ee) => SaveOrder();
        }

        private void InitOrder()
        {
            if (order.Enable != Table.ScheduleS.EnumEnable.待办)
            {
                buttonSave.IsEnabled = false;
                buttonZuoFei.IsEnabled = false;
                buttonUpdate.IsEnabled = false;
            }

            comboBoxFlag.Text = order.Flag.ToString();
            dateTimeUpDownExecuteDateTime.Value = order.ExecuteDateTime;
            textBoxPlannedTasks.Text = order.PlannedTasks;

            if (order.Id == -1)
            {
                buttonZuoFei.Visibility = Visibility.Hidden;
                buttonUpdate.Visibility = Visibility.Hidden;
            }
            else
                buttonSave.Content = "已处理";
        }

        private void UpdateOrder()
        {
                order.Flag = (Table.ScheduleS.EnumFlag)Enum.Parse(typeof(Table.ScheduleS.EnumFlag), comboBoxFlag.Text);
                order.ExecuteDateTime = dateTimeUpDownExecuteDateTime.Value;
                order.PlannedTasks = textBoxPlannedTasks.Text;
                Table.DataTableS.EnumDatabaseStatus result = order.Update();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    this.Close();
                else
                    MessageWindow.Show(result.ToString());
        }

        private void ZuoFeiOrder()
        {
                MessageWindow.Show(order.Update((int)Table.ScheduleS.EnumEnable.作废).ToString());
                this.Close();
        }

        private void SaveOrder()
        {
            if (buttonSave.Content.ToString() == "已处理")
            {
                order.ExecuteDateTime = DateTime.Now;
                order.Update((int)Table.ScheduleS.EnumEnable.结束);
                this.Close();
            }
            else
            {
                order.Flag = (Table.ScheduleS.EnumFlag)Enum.Parse(typeof(Table.ScheduleS.EnumFlag), comboBoxFlag.Text);
                order.ExecuteDateTime = dateTimeUpDownExecuteDateTime.Value;
                order.PlannedTasks = textBoxPlannedTasks.Text;
                Table.DataTableS.EnumDatabaseStatus result = order.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    this.Close();
                else
                    MessageWindow.Show(result.ToString());
            }
        }
    }
}
