﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ProductS.UC
{
    /// <summary>
    /// Interaction logic for TextBox.xaml
    /// </summary>    
    public partial class UCProductS2 : Canvas
    {
        public delegate void Selected(Table.ProductS productS);
        public event Selected SelectedObject;

        private VisualCollection controls;
        private TextBox textBox;
        private ComboBox comboBox;
        private ObservableCollection<DataRow> filterRows = new ObservableCollection<DataRow>();
        private System.Timers.Timer keypressTimer;
        private delegate void TextChangedCallback();
        private bool insertText;
        private int delayTime;
        private int searchThreshold;
      
        public UCProductS2()
        {
            controls = new VisualCollection(this);
            InitializeComponent();

            InitVar();          
        }

        private void InitVar()
        {
            keypressTimer = new System.Timers.Timer();
            keypressTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);

            comboBox = new ComboBox();
            comboBox.IsSynchronizedWithCurrentItem = true;
            comboBox.IsTabStop = false;
            comboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);
            comboBox.ItemsSource = filterRows;
            comboBox.MinWidth = 10;
            comboBox.DisplayMemberPath = "[Name]";

            textBox = new TextBox();
            textBox.MinWidth = 10;
            textBox.TextChanged += new TextChangedEventHandler(textBox_TextChanged);
            textBox.VerticalContentAlignment = VerticalAlignment.Center;

            controls.Add(comboBox);
            controls.Add(textBox);

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            searchThreshold = DoubleHConfig.AppConfig.SearchThreshold;
            delayTime = DoubleHConfig.AppConfig.DelayTime;
        }

        public string Text
        {
            get { return textBox.Text; }
            set
            {
                insertText = true;
                textBox.Text = value;
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (null != comboBox.SelectedItem)
            {
                insertText = true;
                DataRow row = (DataRow)comboBox.SelectedItem;
                textBox.Text = row["Name"].ToString();

                if (SelectedObject != null)
                    SelectedObject(Table.ProductS.GetObject((Int64)row["Id"]));
            }
        }

        private void TextChanged()
        {
            try
            {
                filterRows.Clear();
                if (textBox.Text.Length >= searchThreshold)
                {
                    foreach (DataRow entry in FCNS.Data.SQLdata.GetDataRows("ProductS",new string[]{"Id","Name"},Text,new string[]{"Name","Id","Barcode"}))
                        filterRows.Add(entry);

                    comboBox.IsDropDownOpen = comboBox.HasItems;
                }
                else
                    comboBox.IsDropDownOpen = false;
            }
            catch { }
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                new TextChangedCallback(this.TextChanged));
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (insertText == true) insertText = false;

            else
            {
                if (delayTime > 0)
                {
                    keypressTimer.Interval = delayTime;
                    keypressTimer.Start();
                }
                else TextChanged();
            }
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            textBox.Arrange(new Rect(arrangeSize));
            comboBox.Arrange(new Rect(arrangeSize));
            return base.ArrangeOverride(arrangeSize);
        }

        protected override Visual GetVisualChild(int index)
        {
            return controls[index];
        }

        protected override int VisualChildrenCount
        {
            get { return controls.Count; }
        }
    }
}