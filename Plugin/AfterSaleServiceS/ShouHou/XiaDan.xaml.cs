﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Diagnostics;

namespace AfterSaleServiceS
{
    /// <summary>
    /// PaiDan.xaml 的交互逻辑
    /// </summary>
    public partial class XiaDan : Window
    {
        Table.AfterSaleServiceS order = null;

        public XiaDan(Table.AfterSaleServiceS ass)
        {
            InitializeComponent();

            order = ass;
            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            dateTimePickerXiaDanDateTime.Value = DateTime.Now;

            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.SelectedMemberPath = "Id";
            checkComboBoxUser.ValueMemberPath = "Id";
            checkComboBoxUser.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.经办人);

            textBoxGuanLianDanHao.Text = order.GuanLianDanHao;
            checkBoxFirst.IsChecked = order.First;
            checkBoxFirst.Visibility = order.First ? Visibility.Visible : Visibility.Hidden;
        }

        private void InitEvent()
        {
            buttonOk.Click += (ss, ee) => Save();
            buttonOkAndPrint.Click += (ss, ee) => SaveAndPrint();
        }

        private void SaveAndPrint()
        {
            if (!SaveOrder())
                return;

            PrintFunction.Print(order, DataTableText.售后派单);
            this.Close();
        }

        private void Save()
        {
            if (SaveOrder())
                this.Close();
        }

        private bool SaveOrder()
        {
            if (checkComboBoxUser.SelectedItems.Count == 0)
            {
                MessageWindow.Show("请选择执行工程师");
                return false;
            }
            order.PaiDanDateTime = (DateTime)dateTimePickerXiaDanDateTime.Value;
            order.GuanLianDanHao = textBoxGuanLianDanHao.Text;
            order.ServiceUserS = checkComboBoxUser.SelectedValue;

            Table.DataTableS.EnumDatabaseStatus result = order.Update((int)Table.AfterSaleServiceS.EnumEnable.售后派单);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                return true;

            MessageWindow.Show(result.ToString());
            return false;
        }
    }
}
