﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace AfterSaleServiceS
{
    /// <summary>
    /// PingJia.xaml 的交互逻辑
    /// </summary>
    public partial class PingJia : Window
    {
        public PingJia(Table.AfterSaleServiceS ass)
        {
            InitializeComponent();

            doubleUpDownKaoHeFenShu.Value = Table.SysConfig.SysConfigParams.DefaultKaoHeFenShu;
            textBoxKaoHeYiJian.Focus();

            InitEvent(ass);
        }

        private void InitEvent(Table.AfterSaleServiceS ass)
        {
            checkBoxZuoFei.Click += (ss, ee) => { if (checkBoxZuoFei.IsChecked == true) doubleUpDownKaoHeFenShu.Value = 0; };

            buttonOK.Click += (ss, ee) =>
              {
                  ass.KaoHeFenShu = (double)doubleUpDownKaoHeFenShu.Value;
                  ass.KaoHeYiJian = textBoxKaoHeYiJian.Text;

                  Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
                  if(checkBoxZuoFei.IsChecked == true)
                      result=ass.Update((int)Table.AfterSaleServiceS.EnumEnable.作废);
                  else
                      result = ass.Update((int)Table.AfterSaleServiceS.EnumEnable.员工评价);

                  if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                      this.Close();
                  else
                      MessageWindow.Show(result.ToString());
              };
        }
    }
}
