﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Plugins;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH;
using System.Diagnostics;
using System.Data;

namespace AfterSaleServiceS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class BaoZhang : Window
    {
        Table.AfterSaleServiceS order = null;
        Table.CorS selectCorS = null;
        List<string> guZhangList = new List<string>();

        public BaoZhang(Table.AfterSaleServiceS order)
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            this.order = order ?? new Table.AfterSaleServiceS();
            InitVar();
            InitOrder();
            InitEvent();
            
        }

        private void InitVar()
        {
            if (!Table.SysConfig.SysConfigParams.UseSalesOrderS)
                tabItemSale.Visibility = Visibility.Collapsed;

            corSinfo1.ShowWeiBao = true;
            corSinfo1.Init(true);

            dateTimePickerArrivalDateTime.Value = DateTime.Now;
            dateTimePickerBaoXiuDateTime.Value = DateTime.Now;

            uCUniqueSWeiXiuDuiXiang.Init(Table.UniqueS.EnumFlag.维修对象);
            uCUniqueSWeiXiuLeiXing.Init(Table.UniqueS.EnumFlag.维修类型);
            uCUniqueSKeHuTiGongZiLiao.Init(Table.UniqueS.EnumFlag.客户提供资料);
            uCuserSelect1.Init(Table.UserS.EnumFlag.经办人);
            textBoxGuZhangMiaoShu.Init(Table.AfterSaleServiceS.tableName, new string[] { "GuZhangMiaoShu" }, new string[] { "GuZhangMiaoShu" }, "GuZhangMiaoShu");
        }

        private void InitOrder()
        {
            if (order.Id == -1)
                return;

            corSinfo1.SelectedObjectId = order.CorSId;
            textBoxNote.Text = order.Note;
            dateTimePickerBaoXiuDateTime.Value = order.BaoXiuDateTime;
            dateTimePickerArrivalDateTime.Value = order.ArrivalDateTime;
            uCUniqueSWeiXiuDuiXiang.SelectedObjectId = order.WeiXiuDuiXiang;
            uCUniqueSWeiXiuLeiXing.SelectedObjectId = order.WeiXiuLeiXing;
            uCUniqueSKeHuTiGongZiLiao.SelectedObjectId = order.KeHuTiGongZiLiao;
            textBoxGuZhangMiaoShu.Text = order.GuZhangMiaoShu;
            textBoxJiXing.Text = order.JiXing;
            textBoxJiShenHao.Text = order.JiShenHao;
            textBoxGuanLianDanHao.Text = order.GuanLianDanHao;
            corSinfo1.Contact = order.CorSContactsName;
            corSinfo1.Phone = order.CorSContactsPhone;
            checkBoxFirst.IsChecked = order.First;
            uCuserSelect1.SelectedObjectId = order.XiaDanUserSId;
        }

        private void InitEvent()
        {
            corSinfo1.CorSChanged += (ee) => SelectedCorS(ee); 
            buttonSaveAndNew.Click += (ss, ee) =>  SaveOrderAndPrint();
            buttonSave.Click += (ss, ee) => ClickSave();
        }

        private void SelectedCorS(Table.CorS corS)
        {
            if (corS == null)
            {
                order.CorSId = -1;
                selectCorS = null;

                dataGridSevices.ItemsSource = null;
                dataGridRepair.ItemsSource = null;
                dataGridSales.ItemsSource = null;
                dataGridWeiBao.ItemsSource = null;
            }
            else
            {
                order.CorSId = corS.Id;
                selectCorS = corS;

                dataGridSevices.ItemsSource = AfterSaleServiceSUtility.ServiceRecord.GetList(selectCorS.Id);
                dataGridRepair.ItemsSource = AfterSaleServiceSUtility.RepairRecord.GetList(selectCorS.Id);
                dataGridWeiBao.ItemsSource = AfterSaleServiceSUtility.WeiBaoRecord.GetList(selectCorS.Id);
                if (Table.SysConfig.SysConfigParams.UseSalesOrderS)
                    dataGridSales.ItemsSource = AfterSaleServiceSUtility.SaleRecord.GetList(selectCorS.Id);
            }
        }

        private void SaveOrderAndPrint()
        {
            if (!SaveOrder())
                return;

            PrintFunction.Print(order, DataTableText.售后派单);
            this.Close();
        }

        private bool SaveOrder()
        {
            order.Note = textBoxNote.Text;
            order.OrderDateTime = DateTime.Now;
            order.BaoXiuDateTime = dateTimePickerBaoXiuDateTime.Value == null ? DateTime.Now : (DateTime)dateTimePickerBaoXiuDateTime.Value;
            order.ArrivalDateTime = dateTimePickerArrivalDateTime.Value == null ? DateTime.Now : (DateTime)dateTimePickerArrivalDateTime.Value;
            order.WeiXiuDuiXiang = uCUniqueSWeiXiuDuiXiang.SelectedObjectId;
            order.WeiXiuLeiXing = uCUniqueSWeiXiuLeiXing.SelectedObjectId;
            order.KeHuTiGongZiLiao = uCUniqueSKeHuTiGongZiLiao.SelectedObjectId;
            order.GuZhangMiaoShu = textBoxGuZhangMiaoShu.Text;
            order.JiXing = textBoxJiXing.Text;
            order.JiShenHao = textBoxJiShenHao.Text;
            order.GuanLianDanHao = textBoxGuanLianDanHao.Text;
            order.CorSContactsName = corSinfo1.Contact;
            order.CorSContactsPhone = corSinfo1.Phone;
            order.First = checkBoxFirst.IsChecked.Value;
            order.XiaDanUserSId = uCuserSelect1.SelectedObjectId;
            

            Table.DataTableS.EnumDatabaseStatus result=  Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            return result == Table.DataTableS.EnumDatabaseStatus.操作成功;
        }

        private void ClickSave()
        {
            if (SaveOrder())
                this.Close();
        }
    }
}