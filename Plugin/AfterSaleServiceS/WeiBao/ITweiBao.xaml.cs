﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH.Utility.IO;
using DoubleH.Utility.Configuration;
using System.Collections.ObjectModel;

namespace AfterSaleServiceS
{
    /// <summary>
    /// ITwaiBao.xaml 的交互逻辑
    /// </summary>
    public partial class ITweiBao : Window
    {
        public ITweiBao()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.WeiBaoS order = null;
        public void Init(Table.WeiBaoS obj)
        {
            order = obj ?? new Table.WeiBaoS(Table.WeiBaoS.EnumFlag.外包维保);

            InitOrder();
        }

        private void InitVar()
        {
            uCuserSelect1.Init(Table.UserS.EnumFlag.经办人);
            uCUniqueSLevel.Init(Table.UniqueS.EnumFlag.服务级别定义);
            customerSelect1.Init(true);

            comboBoxItType.ItemsSource = Enum.GetNames(typeof(Table.WeiBaoS.EnumFlag));
        }

        private void InitOrder()
        {
            labelTitle.Content = order.Flag.ToString() + "合同";

            customerSelect1.SelectedObjectId = order.CorSId;
            uCuserSelect1.SelectedValue = order.OperaterId;
            uCUniqueSLevel.SelectedValue = order.UniqueSId;
            textBoxNO.Text = order.OrderNO;
            textBoxNote.Text = order.Note;
            datePickerStart.SelectedDate = order.StartDateTime;
            datePickerEnd.SelectedDate = order.EndDateTime;
            doubleUpDownMoney.Value = order.Money;
            comboBoxItType.Text = order.Flag.ToString();

            dataGridObject.ItemsSource = order.ProductInWeiBaoSList;
            switch (order.Enable)
            {
                case Table.WeiBaoS.EnumEnable.评估:
                    buttonAgain.IsEnabled = false;
                    buttonStop.IsEnabled = false;
                    buttonSave.IsEnabled = false;
                    break;

                case Table.WeiBaoS.EnumEnable.合同生效:
                    buttonShengXiao.IsEnabled = false;
                    break;

                case Table.WeiBaoS.EnumEnable.合同终止:
                    buttonStop.IsEnabled = false;
                    buttonShengXiao.IsEnabled = false;
                    break;
            }
        }

        private void InitEvent()
        {
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            customerSelect1.CorSChanged += (ee) => SelectCorS(ee);
            buttonPrint.Click += (ss, ee) => PrintFunction.Print(order, DataTableText.外包合同);
            buttonStop.Click += (ss, ee) => ZuoFei();
            buttonAgain.Click += (ss, ee) => Again();
            buttonSave.Click += (ss, ee) => Save();
            buttonShengXiao.Click += (ss, ee) => ShengXiao();
            buttonAdd.Click += (ss, ee) => UploadFile();
            buttonRemove.Click += (ss, ee) => RemoveFile();
        }

        private void UploadFile()
        {
            string file = FileTools.GetFile(true);
            if (!string.IsNullOrEmpty(file) || order.Id == -1)
                return;

            TransferFiles.SaveFile(new FileFormat()
            {
                IP = DoubleHConfig.AppConfig.ServerIP,
                Port = DoubleHConfig.AppConfig.ServerPort.ToString(),
                TableName = "WeiBaoS",
                FilePath = file,
                FileName = order.Id + "_" + System.IO.Path.GetFileName(file)
            });
        }

        private void RemoveFile()
        {
            if (listViewFile.SelectedItems.Count == 0)
                return;

            foreach (object obj in listViewFile.SelectedItems)
                TransferFiles.RemoveFile("WeiBaoS", obj.ToString());
        }

        private void SelectCorS(Table.CorS cors)
        {
            order = new Table.WeiBaoS(Table.WeiBaoS.EnumFlag.外包维保);
            order.CorSId = cors.Id;
            InitOrder();
        }

        private void ZuoFei()
        {
            if (MessageWindow.Show("", "确定要终止合同吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            Table.DataTableS.EnumDatabaseStatus result = order.UpdateTo(Table.WeiBaoS.EnumEnable.合同终止);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void Again()
        {
            order = (Table.WeiBaoS)order.CloneObject(null);
            textBoxNO.Clear();
            datePickerStart.SelectedDate = datePickerEnd.SelectedDate;
            datePickerEnd.SelectedDate = datePickerEnd.SelectedDate.Value.AddYears(1);
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;
            buttonShengXiao.IsEnabled = true;
            buttonStop.IsEnabled = false;
            buttonSave.IsEnabled = false;
            buttonAgain.IsEnabled = false;
        }

        private void Save()
        {
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;
            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            result = order.Update();
            MessageWindow.Show(result.ToString());
        }

        private void ShengXiao()
        {
            if (doubleUpDownMoney.Value.Value == 0)
                if (MessageWindow.Show("", "确定合同金额为(0)吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

            //order = new Table.WeiBaoS(Table.WeiBaoS.EnumFlag.外包维保);
            order.Flag = (Table.WeiBaoS.EnumFlag)Enum.Parse(typeof(Table.WeiBaoS.EnumFlag), comboBoxItType.Text);
            order.CorSId = customerSelect1.SelectedObjectId;
            order.OperaterId = uCuserSelect1.SelectedObjectId;

            order.Note = textBoxNote.Text;
            order.OrderNO = textBoxNO.Text;
            order.StartDateTime = datePickerStart.SelectedDate.Value;
            order.EndDateTime = datePickerEnd.SelectedDate.Value;
            order.UniqueSId = (Int64)uCUniqueSLevel.SelectedObjectId;
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;

            order.Money = doubleUpDownMoney.Value.Value;
            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            TextBox textBox = ee.EditingElement as TextBox;
            if (textBox == null)
                return;

            string binding = ((Binding)((DataGridTextColumn)ee.Column).Binding).Path.Path;
            double d = 0;
            switch (binding)
            {
                case "Quantity":
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d <= 0)
                            textBox.Undo();
                    break;
            }
        }
    }
}
