﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Plugins;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections.ObjectModel;

namespace AfterSaleServiceS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class RepairCorS : Window
    {
        Table.RepairS order = null;
        ObservableCollection<Table.ProductSInRepairS> pList = new ObservableCollection<Table.ProductSInRepairS>();
        DataTableText TableText;

        public RepairCorS()
        {
            InitializeComponent();
        }

        public void Init(DataTableText tableText, Table.RepairS obj)
        {
            TableText = tableText;
            this.Title = tableText.ToString();
            switch (tableText)
            {
                case DataTableText.客户坏件送修:
                case DataTableText.客户坏件取回:
                    order = obj ?? new Table.RepairS(true);
                    corSinfo1.Init(true);
                    break;
                case DataTableText.供应商坏件送修:
                case DataTableText.供应商坏件返回:
                    order = obj ?? new Table.RepairS(false);
                    corSinfo1.Init(false);
                    groupBoxShenHe.Header = "供应商坏件返回";
                    break;
                default:
                    System.Diagnostics.Debug.Assert(false);
                    break;
            }

            dateTimePickerSongXiu.Value = DateTime.Now;
            dataGridObject.ItemsSource = pList;
            InitOrder();
            InitEvent();
        }

        private void InitOrder()
        {
            if (order.Enable == Table.RepairS.EnumEnable.客户坏件取回 || order.Enable == Table.RepairS.EnumEnable.供应商坏件返回)
            {
                groupBoxShenHe.IsEnabled = false;
                buttonSave.IsEnabled = false;
                dateTimePickerQuHui.Value = order.EndDateTime;
            }
            else
                dateTimePickerQuHui.Value = DateTime.Now;

            textBoxNote.Text = order.Note;
            dateTimePickerSongXiu.Value = order.StartDateTime;
            doubleUpDownKeHuFeiYong.Value = order.Money;
            corSinfo1.SelectedObjectId = order.CorSId;
            foreach (Table.ProductSInRepairS ps in order.ProductInRepairSList)
                pList.Add(ps);
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) =>  Save(); 
            buttonPrint.Click += (ss, ee) =>  PrintFunction.Print(order,TableText); 
            buttonShenHe.Click += (ss, ee) => ShenHe(); 

            dataGridObject.MouseDoubleClick += (ss, ee) =>  SelectedProducts(ee);
            dataGridObject.CellEditEnding += (ss, ee) =>  CheckCellEditValue(ee);
            dataGridObject.KeyDown += (ss, ee) =>
            {
                if (ee.Key == Key.Delete)
                {
                    if (dataGridObject.SelectedItem != null)
                        pList.Remove((Table.ProductSInRepairS)dataGridObject.SelectedItem);

                }
            };
        }

        private void SelectedProducts(MouseButtonEventArgs ee)
        {
            if ((ee.OriginalSource is TextBlock))
                return;

            ProductS.GetProductS gps = new ProductS.GetProductS();
            gps.Init(ProductS.GetProductS.EnumProductS.可采购商品,null);
            Point p = PointToScreen(Mouse.GetPosition(ee.Source as FrameworkElement));
            gps.Left = p.X;
            gps.Top = p.Y;
            gps.ShowDialog();

            if (gps.Selected == null)
                return;

            IEnumerable<AfterSaleServiceSUtility.SaleRecord> saleRecord = null;
            if (Table.SysConfig.SysConfigParams.FanXiuHasSale)
                saleRecord = AfterSaleServiceSUtility.SaleRecord.GetList(corSinfo1.SelectedObjectId);

            foreach (Table.ProductS ps in gps.Selected)
            {
                bool add = true;
                if (saleRecord != null)
                {//未够严谨,需要修正
                    if (saleRecord.Count<AfterSaleServiceSUtility.SaleRecord>(f => { return f.Name == ps.Name; }) == 0)
                        if (MessageWindow.Show("", "没有'" + ps.Name + "'的销售记录,是否继续?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            add = false;
                }
                if (add)
                    pList.Add(new Table.ProductSInRepairS() { ProductSId = ps.Id });
            }
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            TextBox textBox = ee.EditingElement as TextBox;
            if (textBox == null)
                return;

            string binding = ((Binding)((DataGridTextColumn)ee.Column).Binding).Path.Path;
            double d = 0;
            switch (binding)
            {
                case "Quantity":
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d <= 0)
                            textBox.Undo();
                    break;
            }
        }

        private void Save()
        {
            buttonSave.IsEnabled = false;
            order.CorSId = corSinfo1.SelectedObject.Id;
            order.StartDateTime = dateTimePickerSongXiu.Value.Value;
            order.Note = textBoxNote.Text;
            order.ProductInRepairSList = pList.ToList();

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void ShenHe()
        {
            order.EndDateTime = dateTimePickerQuHui.Value;
            order.Money = doubleUpDownKeHuFeiYong.Value.Value;
            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            result = order.ShenHe();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}
