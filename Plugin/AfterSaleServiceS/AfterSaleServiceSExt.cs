﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace AfterSaleServiceS
{
    class AfterSaleServiceSExt : Plugin
    {
        Window form = null;
        ITweiBao weiBao = null;
        RepairCorS customer = null;

        public override bool Initialize(IPluginHost host)
        {
            switch (TableText)
            {
                case DataTableText.客户坏件送修:
                case DataTableText.客户坏件取回:
                case DataTableText.供应商坏件送修:
                case DataTableText.供应商坏件返回:
                    customer = new RepairCorS();
                    customer.Init(TableText, DataTable as Table.RepairS);
                    customer.ShowDialog();
                    break;

                case DataTableText.外包合同:
                    weiBao = new ITweiBao();
                    weiBao.Init(DataTable as Table.WeiBaoS);
                    weiBao.ShowDialog();
                    break;

                default:
                    Table.AfterSaleServiceS ass = DataTable as Table.AfterSaleServiceS;
                    if (ass == null)
                        form = new BaoZhang(null);
                    else
                    {
                        switch (ass.Enable)
                        {
                            case Table.AfterSaleServiceS.EnumEnable.客户报修:
                                if (Tag!=null && Tag.ToString() == "Edit")
                                    form = new BaoZhang(ass);
                                else
                                    form = new XiaDan(ass);

                                break;
                            case Table.AfterSaleServiceS.EnumEnable.售后派单: form = new WanGong(ass); break;
                            case Table.AfterSaleServiceS.EnumEnable.维护完工: form = new FanKui(ass); break;
                            case Table.AfterSaleServiceS.EnumEnable.回访反馈: form = new PingJia(ass); break;
                            default: return false;
                        }
                    }
                    form.ShowDialog();
                    break;
            }
            Terminate();
            return true;
        }
        public override void Terminate()
        {
            if (form != null)
                form.Close();

            if (weiBao != null)
                weiBao.Close();

            if (customer != null)
                customer.Close();
        }
    }
}
