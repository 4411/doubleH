﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using FCNS.Data;
using System.Data;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace ReportS
{
    /// <summary>
    /// 商品毛利表.xaml 的交互逻辑
    /// </summary>
    public partial class 商品毛利表 : Window
    {
        public 商品毛利表()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            uCdataGrid1.HideChart = true;
            uCdataGrid1.dataGridExt1.LastRowBlod = true;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId(); ;
            buttonOK.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            ObservableCollection<Table.ProductSIO> obj = Table.ProductSIO.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, true, NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue), productS, Table.ProductSIO.EnumEnable.入账);
                
               Table.ProductSIO ps= new Table.ProductSIO()
            {
                _ProductSName = "合计",
                WholesalePrice = Math.Round(obj.Sum(w => w.WholesalePrice), Table.SysConfig.SysConfigParams.DecimalPlaces),
                Quantity=-1,
                //Quantity = Math.Round(obj.Sum(q => q.Quantity), Table.SysConfig.SysConfigParams.DecimalPlaces),
                AveragePrice = Math.Round(obj.Sum(w => w.AveragePrice), Table.SysConfig.SysConfigParams.DecimalPlaces)
            };
               //ps._GrossProfit = ps.WholesalePrice - ps.AveragePrice;
               obj.Add(ps);

            uCdataGrid1.Init(DataTableText.商品毛利表, obj, false);
        }
    }
}