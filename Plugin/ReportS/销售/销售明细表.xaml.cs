﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;

namespace ReportS
{
    /// <summary>
    /// 销售明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 销售明细表 : Window
    {
        public 销售明细表()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.销售订单);
            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.销售退货单);
            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.赠品出库单);

            checkComboBoxEnable.ItemsSource = Enum.GetNames(typeof(Table.SalesOrderS.EnumEnable));
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => { FillData(); };
        }

        private void FillData()
        {
            List<int> store = new List<int>();
            List<int> flag = new List<int>();
            List<int> enable = new List<int>();

            if (!string.IsNullOrEmpty(checkComboBoxStoreS.Text))
                foreach (string str in checkComboBoxStoreS.SelectedValue.Split(','))
                    store.Add(int.Parse(str));

            if (!string.IsNullOrEmpty(checkComboBoxFlag.Text))
                foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                    flag.Add((int)Enum.Parse(typeof(Table.SalesOrderS.EnumFlag), str));

            if (!string.IsNullOrEmpty(checkComboBoxEnable.Text))
                foreach (string str in checkComboBoxEnable.SelectedValue.Split(','))
                    enable.Add((int)Enum.Parse(typeof(Table.SalesOrderS.EnumEnable), str));

            uCdataGrid1.Init(DataTableText.销售明细表,Table.SalesOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, flag.ToArray(), enable.ToArray(), store.ToArray()),
                new UC.CharProperty() { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueName = "Money", ValueTitle = "金额" });
        }

    }
}
