﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ReportS
{
    /// <summary>
    /// 交班明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 交班明细表 : Window
    {
        public 交班明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxPosNO.ItemsSource = Table.PosS.GetList(Table.PosS.EnumEnable.启用, Table.PosS.EnumEnable.停用);
            checkComboBoxPosNO.DisplayMemberPath = "PosNO";
            checkComboBoxPosNO.ValueMemberPath = "PosNO";

            checkComboBoxUser.ItemsSource = Table.UserS.GetList(null, Table.UserS.EnumFlag.POS操作员);
            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.ValueMemberPath = "Id";

            uCdataGrid1.HideChart = true;
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
                {
                    string[] posNO = null;
                    if (!string.IsNullOrEmpty(checkComboBoxPosNO.SelectedValue))
                        posNO = checkComboBoxPosNO.SelectedValue.Split(',');

                    var vr = Table.PosShiftS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, posNO,
                        NormalUtility.FormatStringToInt64(checkComboBoxUser.SelectedValue));

                    uCdataGrid1.Init(DataTableText.交班明细表, vr);
                };
        }
    }
}
