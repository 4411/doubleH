﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections.ObjectModel;

namespace ReportS
{
    /// <summary>
    /// 零售统计表.xaml 的交互逻辑
    /// </summary>
    public partial class 零售统计表 : Window
    {
        public 零售统计表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxPosNO.ItemsSource = Table.PosS.GetList(Table.PosS.EnumEnable.启用, Table.PosS.EnumEnable.停用);
            checkComboBoxPosNO.DisplayMemberPath = "PosNO";
            checkComboBoxPosNO.ValueMemberPath = "PosNO";

            checkComboBoxStore.ItemsSource = MainWindow.allStoreS;
            checkComboBoxStore.DisplayMemberPath = "Name";
            checkComboBoxStore.ValueMemberPath = "Id";

            checkComboBoxUser.ItemsSource = Table.UserS.GetList(null, Table.UserS.EnumFlag.POS操作员);
            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.ValueMemberPath = "Id";

            checkComboBoxType.ItemsSource = Enum.GetNames(typeof(Table.PosOrderS.EnumFlag));
        }

        private void InitEvent()
        {
            radioButtonFlag.Checked += (ss, ee) => uCDateTime1.CanFilterByDate = radioButtonDate.IsChecked.Value;
           radioButtonStoreS.Checked += (ss, ee) => uCDateTime1.CanFilterByDate = radioButtonDate.IsChecked.Value;
            radioButtonUserS.Checked += (ss, ee) => uCDateTime1.CanFilterByDate = radioButtonDate.IsChecked.Value;
            radioButtonPosNO.Checked += (ss, ee) => uCDateTime1.CanFilterByDate = radioButtonDate.IsChecked.Value;
            radioButtonDate.Checked += (ss, ee) => uCDateTime1.CanFilterByDate = radioButtonDate.IsChecked.Value;
            buttonOK.Click += (ss, ee) => Search();
        }

        private void Search()
        {
            string[] posNO = null;
            if (!string.IsNullOrEmpty(checkComboBoxPosNO.SelectedValue))
                posNO = checkComboBoxPosNO.SelectedValue.Split(',');

            List<Table.PosOrderS.EnumFlag> flag = new List<Table.PosOrderS.EnumFlag>();
            if (!string.IsNullOrEmpty(checkComboBoxType.SelectedValue))
                foreach (string str in checkComboBoxType.SelectedValue.Split(','))
                    flag.Add((Table.PosOrderS.EnumFlag)Enum.Parse(typeof(Table.PosOrderS.EnumFlag), str));

            ObservableCollection<Table.PosOrderS> temp = Table.PosOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,
                NormalUtility.FormatStringToInt64(checkComboBoxStore.SelectedValue), posNO,
                NormalUtility.FormatStringToInt64(checkComboBoxUser.SelectedValue), flag.ToArray());

            Filter(temp);
        }

        private void Filter(ObservableCollection<Table.PosOrderS> obj)
        {
            IEnumerable<TempClass> temp=null;
            if (radioButtonFlag.IsChecked.Value)
                temp = FilterByFlag(obj);
            else if (radioButtonStoreS.IsChecked.Value)
                temp = FilterByStoreS(obj);
            else if (radioButtonUserS.IsChecked.Value)
                temp = FilterByUserS(obj);
            else if (radioButtonPosNO.IsChecked.Value)
                temp = FilterByPosNO(obj);
            else
                temp = FilterByDate(obj);

            uCdataGrid1.Init(DataTableText.零售统计表,temp, true, new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "金额", ValueName = "_Money" });

        }

        private List<TempClass> FilterByFlag(ObservableCollection<Table.PosOrderS> obj)
        {
            List<TempClass> tc = new List<TempClass>();
            foreach (Table.PosOrderS ps in obj.Distinct(new NormalUtility.Compare<Table.PosOrderS>((x, y) => x.Flag == y.Flag)))
            {
                TempClass t = new TempClass();
                t._Name = ps.Flag.ToString();
                t._Money = obj.Where(w=>w.Flag==ps.Flag).Sum(f => f.Money);

                tc.Add(t);
            }
            return tc;
        }

        private List<TempClass> FilterByStoreS(ObservableCollection<Table.PosOrderS> obj)
        {
            List<TempClass> tc = new List<TempClass>();
            foreach (Table.PosOrderS ps in obj.Distinct(new NormalUtility.Compare<Table.PosOrderS>((x, y) => x.StoreSId == y.StoreSId)))
            {
                TempClass t = new TempClass();
                t._Name = ps._StoreSName;
                t._Money = obj.Where(w => w.StoreSId==ps.StoreSId).Sum(f => f.Money);

                tc.Add(t);
            }
            return tc;
        }

        private List<TempClass> FilterByUserS(ObservableCollection<Table.PosOrderS> obj)
        {
            List<TempClass> tc = new List<TempClass>();
            foreach (Table.PosOrderS ps in obj.Distinct(new NormalUtility.Compare<Table.PosOrderS>((x, y) => x.UserSId == y.UserSId)))
            {
                TempClass t = new TempClass();
                t._Name = ps._UserSName;
                t._Money = obj.Where(w => w.UserSId == ps.UserSId).Sum(f => f.Money);

                tc.Add(t);
            }
            return tc;
        }

        private List<TempClass> FilterByPosNO(ObservableCollection<Table.PosOrderS> obj)
        {
            List<TempClass> tc = new List<TempClass>();
            foreach (Table.PosOrderS ps in obj.Distinct(new NormalUtility.Compare<Table.PosOrderS>((x, y) => x.PosNO == y.PosNO)))
            {
                TempClass t = new TempClass();
                t._Name = ps.PosNO;
                t._Money = obj.Where(w => w.PosNO == ps.PosNO).Sum(f => f.Money);

                tc.Add(t);
            }
            return tc;
        }

        private List<TempClass> FilterByDate(ObservableCollection<Table.PosOrderS> obj)
        {
            List<TempClass> tc = new List<TempClass>();
            Dictionary<string, List<object>> result = uCDateTime1.FilterData(obj);
            if (result != null)
            {
                foreach (KeyValuePair<string, List<object>> kp in result)
                {
                    TempClass t = new TempClass();
                    t._Name = kp.Key;
                    foreach (Table.PosOrderS ps in kp.Value)
                        t._Money += ps.Money;

                    tc.Add(t);
                }
            }
            return tc;
        }



        private class TempClass
        {
            public string _Name { get; set; }
            public double _Money { get; set; }
        }
    }
}