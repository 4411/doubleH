﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;

namespace ReportS
{
    /// <summary>
    /// 故障库.xaml 的交互逻辑
    /// </summary>
    public partial class 故障库 : Window
    {
        public 故障库()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxEnable.DisplayMemberPath = "Name";
            checkComboBoxEnable.ValueMemberPath = "Id";
            checkComboBoxEnable.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.维修对象);
            checkComboBoxFlag.DisplayMemberPath = "Name";
            checkComboBoxFlag.ValueMemberPath = "Id";
            checkComboBoxFlag.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.维修类型);
            uCdataGrid1.Init(DataTableText.故障库, null);
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => GetData();
        }

        private void GetData()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select GuZhangMiaoShu,JieJueFangFa  from AfterSaleServiceS where ");
            sb.Append("(cast(OrderDateTime as varchar)>='" + uCDateTime1.StartDateTime.ToString(FCNS.Data.DbDefine.dateTimeFormat) + "'");
            sb.Append(" and cast(OrderDateTime as varchar)<='" + uCDateTime1.EndDateTime.ToString(FCNS.Data.DbDefine.dateTimeFormat) + "')");

            if (!string.IsNullOrEmpty(checkComboBoxFlag.Text))
            {
                sb.Append(" and (");
                foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                    sb.Append("WeiXiuLeiXing=" + (int)Enum.Parse(typeof(Table.UniqueS.EnumFlag), str) + " or ");

                sb.Remove(sb.Length - 3, 3);
                sb.Append(")");
            }

            if (!string.IsNullOrEmpty(checkComboBoxEnable.Text))
            {
                sb.Append(" and (");
                foreach (string str in checkComboBoxEnable.SelectedValue.Split(','))
                    sb.Append("WeiXiuDuiXiang=" + (int)Enum.Parse(typeof(Table.UniqueS.EnumFlag), str) + " or ");

                sb.Remove(sb.Length - 3, 3);
                sb.Append(")");
            }

            List<TempClass> obj = new List<TempClass>();
            foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows(sb.ToString()))
            {
                TempClass tc = new TempClass();
                tc._GuZhangMiaoShu = row[0] as string;
                tc._JieJueFangFa = row[1] as string;
                obj.Add(tc);
            }
            uCdataGrid1.Init(DataTableText.故障库,obj);
        }



        public class TempClass
        {
            public string _WeiXiuDuiXiang { get; set; }
            public string _WeiXiuLeiXing { get; set; }
            public string _GuZhangMiaoShu { get; set; }
            public string _JieJueFangFa { get; set; }
        }
    }
}