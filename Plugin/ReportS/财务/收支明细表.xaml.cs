﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 收支明细.xaml 的交互逻辑
    /// </summary>
    public partial class 收支明细表 : Window
    {
        public 收支明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxFlag.ItemsSource = Enum.GetNames(typeof(Table.PayS.EnumFlag));

            checkComboBoxBank.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.支付方式);
            checkComboBoxBank.DisplayMemberPath = "Name";
            checkComboBoxBank.ValueMemberPath = "Id";

            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;
            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
            {
                List<Table.PayS.EnumFlag> flag = new List<Table.PayS.EnumFlag>();
                if(!string.IsNullOrEmpty(checkComboBoxFlag.SelectedValue))
                    foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                        flag.Add((Table.PayS.EnumFlag)Enum.Parse(typeof(Table.PayS.EnumFlag), str));
              
                IList temp = Table.PayS.GetList(Table.PayS.EnumEnable.审核,uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, 
                    NormalUtility.FormatStringToInt64(checkComboBoxBank.SelectedValue),
                     NormalUtility.FormatStringToInt64(checkComboBoxCorS.SelectedValue), flag.ToArray());

                List<TempFunction> tf = new List<TempFunction>();
                    PayData(tf, temp);

                    uCdataGrid1.Init(DataTableText.收支明细表, tf, new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "收入", ValueName = "MoneyIn" },
                    new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "支出", ValueName = "MoneyOut" });
            };
        }

        private void PayData(List<TempFunction> tf, IList ps)
        {
            foreach (Table.PayS p in ps)
            {
                //if (p.RealMoney == 0)//没钱收的单就不显示了
                //    continue;

                TempFunction t = new TempFunction();
                t.OrderDateTime = p.OrderDateTime.Value;
                t.Note = p.Note;
                t._CorSName = p._CorSName;
                t._BankName = p._BankName;

                if (p.Flag == Table.PayS.EnumFlag.付款单 || p.Flag == Table.PayS.EnumFlag.其它费用支出单)
                {
                    t.Money = -p.Money;
                    t.MoneyOut = -p.RealMoney;
                }
                else
                {
                    t.Money = p.Money;
                    t.MoneyIn = p.RealMoney;
                }

                t.Source = GetOrderTypeName(p.RelatedOrderType);
                tf.Add(t);
            }
        }

        private string GetOrderTypeName(string RelatedOrderType)
        {
            switch (RelatedOrderType)
            {
                case Table.PurchaseOrderS.tableName: return "采购款";
                case Table.SalesOrderS.tableName: return "批发款";
                case Table.AfterSaleServiceS.tableName: return "售后款";
                case Table.ProjectS.tableName: return "项目款";
                case Table.CarOilS.tableName:
                case Table.CarHealthS.tableName:
                    return "车辆成本";
                case Table.WuLiuS.tableName: return "物流费用";

                default: return "其它收支款";
            }
        }

        class TempFunction
        {
            public DateTime OrderDateTime { get; set; }
            public double? MoneyIn { get; set; }
            public double? MoneyOut { get; set; }
            public string Note { get; set; }
            public string Source { get; set; }
            public double? Money { get; set; }
            public string _CorSName { get; set; }
            public string _BankName { get; set; }
        }
    }
}