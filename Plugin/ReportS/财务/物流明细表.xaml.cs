﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ReportS
{
    /// <summary>
    /// 物流明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 物流明细表 : Window
    {
        public 物流明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;

            checkComboBoxWuLiuS.DisplayMemberPath = "Name";
            checkComboBoxWuLiuS.ValueMemberPath = "WuLiuName";
            checkComboBoxWuLiuS.ItemsSource = Table.CorS.GetWuLiuList();
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) =>
                {
                   uCdataGrid1.Init(DataTableText.物流明细表, Table.WuLiuS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,checkComboBoxWuLiuS.SelectedValue.Split(','), NormalUtility.FormatStringToInt64(checkComboBoxCorS.SelectedValue)),
                        new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "金额", ValueName = "Money" });
                };
        }


        public class TempClass 
        {

        }
    }
}
