﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DoubleH.Plugins;
using System.Windows;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CorS
{
    public class CorSExt : Plugin
    {
        private IPluginHost m_host = null;
        MainWindow window = null;
        WindowVip vip = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
                case DataTableText.客户:
                case DataTableText.供应商:
                    window = new MainWindow();
                    window.Init((TableText == DataTableText.客户) ? true : false, DataTable as Table.CorS);
                    window.ShowDialog();
                    break;

                case DataTableText.物流公司:
                    window = new MainWindow();
                    window.Init(false, DataTable as Table.CorS, true);
                    window.ShowDialog();
                    break;

                case DataTableText.会员资料:
                    vip = new WindowVip();
                    vip.Init(DataTable as Table.CorS);
                    vip.ShowDialog();
                    break;
            }

            Terminate();
            return true;
        }
        public override void Terminate()
        {
            if (window != null)
                window.Close();

            if (vip != null)
                vip.Close();
        }
    }
}