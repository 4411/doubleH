﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using DoubleH.Plugins;
using DoubleH.Utility;
using System.Collections.ObjectModel;
using System.Data;
using System.Collections;

namespace CorS
{
    /// <summary>
    /// GetCustomerS.xaml 的交互逻辑
    /// </summary>
    public partial class GetCorS : Window
    {
        bool? isCustomerS = true;
        /// <summary>
        /// null  所有
        /// </summary>
        /// <param name="customer"></param>
        public GetCorS(bool? customer,bool multiSelect)
        {
            InitializeComponent();

            this.isCustomerS = customer;
            isSelected.Visibility = multiSelect ? Visibility.Visible : Visibility.Collapsed;

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            InitGroup();
            textBox1.Focus();
            DataTableText tableText = isCustomerS.Value ? DataTableText.客户 : DataTableText.供应商;
            buttonNew.IsEnabled = Table.UserS.LoginUser.GetAuthority(tableText) > Table.UserS.EnumAuthority.查看;
        }

        private void InitEvent()
        {
            dataGridObject.LoadingRow += (ss, ee) => ee.Row.Header = ee.Row.GetIndex() + 1; 
            checkBoxEnable.Click += (ss, ee) => InitGroup();
            treeViewGroup.MouseLeftButtonUp += (ss, ee) => SearchByGroup();
            textBox1.KeyDown += (ss, ee) => SearchByEnter(ee);
            dataGridObject.MouseDoubleClick += (ss, ee) => DoubleClickSelectCorS();
            buttonNew.Click += (ss, ee) =>AddNewCorS();
        }

        private void SearchByGroup()
        {
            if (treeViewGroup.SelectedItem == null)
                return;

            Int64 groupSId = ((GroupS.GroupSUtility.GroupSBinding)treeViewGroup.SelectedItem).Id;
            TextBoxSearch(string.Empty, groupSId);
        }

        private void SearchByEnter(KeyEventArgs ee)
        {
            if (ee.Key == Key.Enter)
                TextBoxSearch(textBox1.Text, null);
        }

        private void DoubleClickSelectCorS()
        {
            if (dataGridObject.SelectedItem == null)
                return;

            customer = dataGridObject.SelectedItem as Table.CorS;
            this.Close();
        }

        private void AddNewCorS()
        {
            MainWindow mw = new MainWindow();
            mw.Init((isCustomerS.HasValue ? isCustomerS.Value : true), null);
            mw.ShowDialog();
        }

        private void InitGroup()
        {
            Table.GroupS.EnumEnable? enable = Table.GroupS.EnumEnable.启用;
            if (checkBoxEnable.IsChecked.Value)
                enable = null;

            List<GroupS.GroupSUtility.GroupSBinding> groupBinding = new List<GroupS.GroupSUtility.GroupSBinding>();
            ObservableCollection<Table.GroupS> allGroupS = Table.GroupS.GetList(Table.GroupS.EnumFlag.客商分类, enable);
            foreach (Table.GroupS gs in allGroupS.Where(f => f.ParentId == -1))
            {
                GroupS.GroupSUtility.GroupSBinding gsb = new GroupS.GroupSUtility.GroupSBinding() { Id = gs.Id, Name = gs.Name, ParentId = gs.ParentId, GroupSBindings = new List<GroupS.GroupSUtility.GroupSBinding>() };
                groupBinding.Add(gsb);
                FindGroup(gsb, allGroupS);
            }

            treeViewGroup.ItemsSource = groupBinding;
        }

        private void FindGroup(GroupS.GroupSUtility.GroupSBinding group, ObservableCollection<Table.GroupS> allGroup)
        {
            IEnumerable gs = allGroup.Where(f => f.ParentId == group.Id);
            if (gs == null)
                return;

            foreach (Table.GroupS g in gs)
            {
                GroupS.GroupSUtility.GroupSBinding gsb = new GroupS.GroupSUtility.GroupSBinding() { Id = g.Id, Name = g.Name, ParentId = g.ParentId, GroupSBindings = new List<GroupS.GroupSUtility.GroupSBinding>() };
                group.GroupSBindings.Add(gsb);

                FindGroup(gsb, allGroup);
            }
        }

        Table.CorS customer = null;
        /// <summary>
        /// 获取已选择的客户或供应商
        /// </summary>
        public Table.CorS Selected
        {
            get { return customer; }
        }

        private void TextBoxSearch(string searchText, Int64? groupSId)
        {
            List<Table.CorS.EnumFlag> flag=new List<Table.CorS.EnumFlag>();
            if(isCustomerS.HasValue)
            {    if(isCustomerS.Value)
                     flag.Add(Table.CorS.EnumFlag.客户);
                else
                    flag.Add(Table.CorS.EnumFlag.供应商);

                flag.Add(Table.CorS.EnumFlag.客户和供应商);
            }
          dataGridObject.ItemsSource=Table.CorS.GetList(searchText,groupSId,flag.ToArray());
            textBox1.Clear();
        }
    }
}