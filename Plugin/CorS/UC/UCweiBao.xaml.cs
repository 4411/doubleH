﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CorS
{
    /// <summary>
    /// UCweiBao.xaml 的交互逻辑
    /// </summary>
    public partial class UCweiBao : UserControl
    {
        public UCweiBao()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 设置维保的日期
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public void SetDate(DateTime start, DateTime end,string title)
        {
            label1.Content = title;
            dateTimePickerStart.Value = start;
            dateTimePickerEnd.Value = end;
        }
    }
}
