﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace CorS
{
    /// <summary>
    /// CustomerSinfo.xaml 的交互逻辑
    /// </summary>
    public partial class UCCorSinfo : UserControl
    {
        public UCCorSinfo()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        public delegate void ChangedCorS(Table.CorS css);
        public event ChangedCorS CorSChanged;
        bool isCustomer = false;
        /// <summary>
        /// 显示客商选择框,默认显示
        /// </summary>
        public bool ShowCorSelect
        {
            get { return corSelect1.IsEnabled; }
            set { corSelect1.IsEnabled = value; }
        }

        Table.CorS CorS = null;
        /// <summary>
        /// 获取或设置已选择的客商
        /// </summary>
        public Table.CorS SelectedObject
        {
            get { return CorS; }
            set
            {
                CorS = value;
                corSelect1.SelectedObject = CorS;
                SelectedCorS();
            }
        }
        /// <summary>
        /// 获取或设置已选择的客商Id
        /// </summary>
        public Int64 SelectedObjectId
        {
            get
            {
                if (CorS == null)
                    return -1;
                else
                    return CorS.Id;
            }
            set
            {
                SelectedObject = Table.CorS.GetObject(value);
            }
        }

        bool showWeiBao = false;
        /// <summary>
        /// 是否显示维保标记,默认不显示
        /// </summary>
        public bool ShowWeiBao
        {
            set { showWeiBao = value; }
        }

        /// <summary>
        /// 获取或设置联系人名称
        /// </summary>
        public string Contact { get { return comboBoxContact.Text; } set { comboBoxContact.Text = value; } }
        
        /// <summary>
        /// 获取或设置联系人的手机
        /// </summary>
        public string Phone { get { return textBoxCustomerSPhone.Text; } set { textBoxCustomerSPhone.Text = value; } }

        public void Init(bool isCustomer)
        {
            this.isCustomer = isCustomer;
            corSelect1.Init(isCustomer);
        }

        private void InitVar()
        {
            uCUniqueSArea.Init(Table.UniqueS.EnumFlag.区域);

            checkComboBoxKeHuXingZhi.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.客户性质,Table.UniqueS.EnumEnable.启用);
            checkComboBoxKeHuXingZhi.DisplayMemberPath = "Name";
            checkComboBoxKeHuXingZhi.SelectedMemberPath = "Id";
            checkComboBoxKeHuXingZhi.ValueMemberPath = "Id";

            buttonUpdateCorS.IsEnabled = Table.UserS.LoginUser.GetAuthority(isCustomer?DataTableText.客户:DataTableText.供应商)>Table.UserS.EnumAuthority.编辑;
        }

        private void InitEvent()
        {
            corSelect1.CorSChanged += (ee) => SelectCorS(ee);
            comboBoxContact.SelectionChanged += (ss, ee) => SelectContact();
            buttonUpdateCorS.Click += (ss, ee) => UpdateCorS(); 
        }

        private void SelectCorS(Table.CorS ee)
        {
            CorS = ee;
            SelectedCorS();
            if (CorSChanged != null)
                CorSChanged(ee);
        }

        private void SelectContact()
        {
            Table.UserS info = comboBoxContact.SelectedItem as Table.UserS;
            if (info != null)
                textBoxCustomerSPhone.Text = info.Phone;
        }

        private void SelectedCorS()
        {
            if (CorS == null)
                return;

            comboBoxContact.ItemsSource = CorS.AllContact;
            comboBoxContact.DisplayMemberPath= "Name";
            if (comboBoxContact.HasItems)
                comboBoxContact.SelectedIndex = 0;

            textBoxCustomerSAddress.Text = CorS.Address;
            textBoxCustomerSTel.Text = CorS.Tel;
            uCUniqueSArea.SelectedObjectId = CorS.AreaSId;
            checkComboBoxKeHuXingZhi.SelectedValue = CorS.KeHuXingZhi;
            if (!showWeiBao)
                return;

            Table.WeiBaoS ws = Table.WeiBaoS.IsWeiBaoNow(CorS.Id);
            if (ws == null)
                uCweiBao1.Visibility = Visibility.Hidden;
            else
            {
                uCweiBao1.Visibility = Visibility.Visible;
                uCweiBao1.SetDate(ws.StartDateTime, ws.EndDateTime, ws._UniqueSName);
            }
        }

        private void UpdateCorS()
        {
            if (CorS == null)
                return;

            CorS.Name = corSelect1.textBoxExtText.Text;
            CorS.Tel = textBoxCustomerSTel.Text;
            CorS.Address = textBoxCustomerSAddress.Text;
            CorS.AreaSId = uCUniqueSArea.SelectedObjectId;
            CorS.KeHuXingZhi = checkComboBoxKeHuXingZhi.SelectedValue;

            Table.UserS user=comboBoxContact.SelectedValue  as Table.UserS;
            if (user == null)
                CorS.AllContact.Add(new Table.UserS() { Name = comboBoxContact.Text, Phone = textBoxCustomerSPhone.Text });
            else
            {
                user.Name = comboBoxContact.Text;
                user.Phone = textBoxCustomerSPhone.Text;
            }

            MessageWindow.Show(CorS.UpdateCorSInfo().ToString());
        }
    }
}