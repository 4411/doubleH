﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Table = FCNS.Data.Table;
using System.Data;

namespace CorS.UC
{
    public class UCCorS : Xceed.Wpf.Toolkit.CheckComboBox
    {
        class NameAndValue
        {
            public string Name { get; set; }
            public Int64 Id { get; set; }
        }
        public UCCorS()
        {
            InitVar();
        }

        private void InitVar()
        {
            this.DisplayMemberPath = "Name";
            this.ValueMemberPath = "Id";
        }

        /// <summary>
        /// 必须调用me 初始化控件参数
        /// </summary>
        /// <param name="obj"></param>
        public void Init(params Table.CorS.EnumFlag[] flag)
        {
            StringBuilder sb = new StringBuilder("select Id,Name from CorS ");
            if (flag.Length != 0)
            {
                sb.Append(" where ");
                foreach (int i in flag)
                    sb.Append(" Flag=" + i + " or ");

                sb.Remove(sb.Length - 3, 3);
            }
            List<NameAndValue> nav = new List<NameAndValue>();
            foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows(sb.ToString()))
                nav.Add(new NameAndValue() { Id = (Int64)row["Id"], Name = row["Name"] as string });

            this.ItemsSource = nav;
        }
    }
}
