﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace ProjectS
{
    /// <summary>
    /// ScheduleWindow.xaml 的交互逻辑
    /// </summary>
    public partial class FeedbackWindow : Window
    {
        public FeedbackWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.FeedbackInProjectS order = null;

        public void Init(Table.FeedbackInProjectS obj)
        {
            order = obj ?? new Table.FeedbackInProjectS();
            InitOrder();
            textBoxName.Focus();
        }

        private void InitVar()
        {
            comboBoxProjectS.ItemsSource = Table.ProjectS.GetList(Table.ProjectS.EnumEnable.实施, Table.ProjectS.EnumEnable.完工);
            comboBoxProjectS.DisplayMemberPath = "Name";
            comboBoxProjectS.SelectedValuePath = "Id";

            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.SelectedMemberPath = "Id";
            checkComboBoxUser.ValueMemberPath = "Id";
            checkComboBoxUser.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.经办人);

            dateTimePickerOrder.Value = DateTime.Now;

            comboBoxLevel.ItemsSource = Enum.GetNames(typeof(Table.FeedbackInProjectS.EnumLevel));
            comboBoxLevel.SelectedIndex = 0;
        }

        private void InitOrder()
        {
            if (order.Id == -1)
                return;

            textBoxName.Text = order.Name;
            checkComboBoxUser.SelectedValue = order.OperatorerSId;
            textBoxNote.Text = order.Note;
            comboBoxProjectS.SelectedValue = order.ProjectSId;
            comboBoxLevel.Text = order.Level.ToString();
            dateTimePickerOrder.Value = order.OrderDateTime;

            buttonSave.IsEnabled = order.Enable == Table.FeedbackInProjectS.EnumEnable.评估;
            buttonShiShi.IsEnabled = order.Enable == Table.FeedbackInProjectS.EnumEnable.评估;
            buttonWanGong.IsEnabled = order.Enable == Table.FeedbackInProjectS.EnumEnable.实施;
            buttonZuoFei.IsEnabled = (order.Enable == Table.FeedbackInProjectS.EnumEnable.评估 || order.Enable == Table.FeedbackInProjectS.EnumEnable.实施);
            uCUserSelect1.SelectedObjectId = order.UserSId;
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            buttonShiShi.Click += (ss, ee) => UpdateOrder(Table.FeedbackInProjectS.EnumEnable.实施);
            buttonWanGong.Click += (ss, ee) => UpdateOrder(Table.FeedbackInProjectS.EnumEnable.完成);
            buttonZuoFei.Click += (ss, ee) => UpdateOrder(Table.FeedbackInProjectS.EnumEnable.作废);
            comboBoxProjectS.SelectionChanged += (ss, ee) => ProjectSelectionChanged();
            uCUserSelect1.SelectionChanged += (ss, ee) => CorSUserChanged();
        }

        private void CorSUserChanged()
        {
            textBoxPhone.Text = uCUserSelect1.SelectedObjectPhone;
        }

        private void ProjectSelectionChanged()
        {
            Table.ProjectS ps = comboBoxProjectS.SelectedItem as Table.ProjectS;
            if (ps == null)
                return;

            if (order.Enable > Table.FeedbackInProjectS.EnumEnable.实施)
                uCUserSelect1.Init(ps.CorSId);
            else
                uCUserSelect1.Init(ps.CorSId, Table.UserS.EnumEnable.启用);
        }

        private void Save()
        {
            if (comboBoxProjectS.SelectedItem != null)
                order.ProjectSId = (Int64)comboBoxProjectS.SelectedValue;

            NewUser();
            order.Name = textBoxName.Text;
            order.OperatorerSId = checkComboBoxUser.SelectedValue;
            order.Note = textBoxNote.Text;
            order.OrderDateTime = dateTimePickerOrder.Value.Value;
            order.Level = (Table.FeedbackInProjectS.EnumLevel)Enum.Parse(typeof(Table.FeedbackInProjectS.EnumLevel), comboBoxLevel.Text);

            Table.DataTableS.EnumDatabaseStatus result = order.Id == -1 ? order.Insert() : order.Update();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }

        private void NewUser()
        {
            if (uCUserSelect1.SelectedObject != null)
            {
                order.UserSId = uCUserSelect1.SelectedObjectId;
                return;
            }
            if (string.IsNullOrEmpty(uCUserSelect1.Text))
                return;

            Table.UserS user = new Table.UserS();
            order.UserSId = user.InsertForCorS((Int64)FCNS.Data.SQLdata.ExecuteScalar("select CorSId from ProjectS where Id=" + order.ProjectSId),
                uCUserSelect1.Text,textBoxPhone.Text);
        }

        private void UpdateOrder(Table.FeedbackInProjectS.EnumEnable enable)
        {
            Table.DataTableS.EnumDatabaseStatus result = order.UpdateTo(enable);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }
    }
}