﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace ProjectS
{
    /// <summary>
    /// ScheduleWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ScheduleWindow : Window
    {
        public ScheduleWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.ScheduleInProjectS order = null;
        ObservableCollection<Table.UniqueS> allSchedule = null;

        public void Init(Table.ScheduleInProjectS obj)
        {
            order = obj ?? new Table.ScheduleInProjectS();
            InitOrder();
            textBoxName.Focus();
        }

        private void InitVar()
        {
            comboBoxProjectS.ItemsSource = Table.ProjectS.GetList(Table.ProjectS.EnumEnable.评估,Table.ProjectS.EnumEnable.实施);
            comboBoxProjectS.DisplayMemberPath = "Name";
            comboBoxProjectS.SelectedValuePath = "Id";
            
            comboBoxSchedule.DisplayMemberPath = "Name";
            comboBoxSchedule.SelectedValuePath = "Id";

            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.SelectedMemberPath = "Id";
            checkComboBoxUser.ValueMemberPath = "Id";
            checkComboBoxUser.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.经办人);
        }

        private void InitOrder()
        {

            if (order.Id == -1)
            {
                allSchedule = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.任务类型, Table.UniqueS.EnumEnable.启用);
                return;
            }

                allSchedule = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.任务类型);

            textBoxName.Text = order.Name;
            checkComboBoxUser.SelectedValue = order.OperatorerSId;
            dateTimePickerEnd.Value = order.EndDateTime;
            dateTimePickerStart.Value = order.StartDateTime;
            textBoxNote.Text = order.Note;
            comboBoxProjectS.SelectedValue = order.ProjectSId;
            comboBoxSchedule.SelectedValue = order.ScheduleId;

            buttonSave.IsEnabled = order.Enable == Table.ScheduleInProjectS.EnumEnable.评估;
            buttonShiShi.IsEnabled = order.Enable == Table.ScheduleInProjectS.EnumEnable.评估;
            buttonWanGong.IsEnabled = order.Enable == Table.ScheduleInProjectS.EnumEnable.实施;
            buttonZuoFei.IsEnabled = (order.Enable == Table.ScheduleInProjectS.EnumEnable.评估 || order.Enable == Table.ScheduleInProjectS.EnumEnable.实施);
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            comboBoxProjectS.SelectionChanged += (ss, ee) => ProjectSelectionChanged();
            buttonShiShi.Click += (ss, ee) => UpdateOrder(Table.ScheduleInProjectS.EnumEnable.实施);
            buttonWanGong.Click += (ss, ee) => UpdateOrder(Table.ScheduleInProjectS.EnumEnable.完成);
            buttonZuoFei.Click += (ss, ee) => UpdateOrder(Table.ScheduleInProjectS.EnumEnable.作废);
        }

        private void ProjectSelectionChanged()
        {
            Table.ProjectS ps=comboBoxProjectS.SelectedItem as Table.ProjectS;
            if(ps==null)
                return;

            string sch = ps.Schedule;
            var vr = from f in allSchedule where sch.Contains(f.Id.ToString()) select f;
            comboBoxSchedule.ItemsSource = vr;
        }

        private void Save()
        {
            order.Name = textBoxName.Text;
            if (comboBoxProjectS.SelectedItem != null)
                order.ProjectSId = (Int64)comboBoxProjectS.SelectedValue;

            order.OperatorerSId = checkComboBoxUser.SelectedValue;
            order.EndDateTime = dateTimePickerEnd.Value;
            order.StartDateTime = dateTimePickerStart.Value;
            order.Note = textBoxNote.Text;
            if (comboBoxSchedule.SelectedItem != null)
                order.ScheduleId = (Int64)comboBoxSchedule.SelectedValue;

            Table.DataTableS.EnumDatabaseStatus result = order.Id == -1 ? order.Insert() : order.Update();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }

        private void UpdateOrder(Table.ScheduleInProjectS.EnumEnable enable)
        {
            order.EndDateTime = dateTimePickerEnd.Value;
            order.StartDateTime = dateTimePickerStart.Value;
            order.OperatorerSId = checkComboBoxUser.SelectedValue;
            order.Note = textBoxNote.Text;

            Table.DataTableS.EnumDatabaseStatus result = order.UpdateTo(enable);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }
    }
}