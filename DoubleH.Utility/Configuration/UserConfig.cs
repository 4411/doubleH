﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using DoubleH.Utility;
using System.Windows.Media;

namespace DoubleH.Utility.Configuration
{
    [Serializable]
    public class UserConfig
    {
        public UserUIparams GetUserUIparams(DataTableText tableText)
        {
            UserUIparams us = DataGridBinding.Find(f => f.TableText == tableText);
            if (us == null)
            {
                us = new UserUIparams() { TableText = tableText };
                DataGridBinding.Add(us);
            }
            return us;
        }
        
        bool scheduleSUseImageForFlag = false;
        /// <summary>
        /// 是否使用图形化的任务标记
        /// </summary>
        public bool ScheduleSUseImageForFlag
        {
            get { return scheduleSUseImageForFlag; }
            set { scheduleSUseImageForFlag = value; }
        }

        string scheduleImage0 = string.Empty;
        public string ScheduleImage0
        {
            get { return scheduleImage0; }
            set { scheduleImage0 = value; }
        }

        string scheduleImage1 = string.Empty;
        public string ScheduleImage1
        {
            get { return scheduleImage1; }
            set { scheduleImage1 = value; }
        }

        string scheduleImage2 = string.Empty;
        public string ScheduleImage2
        {
            get { return scheduleImage2; }
            set { scheduleImage2 = value; }
        }

        string scheduleImage3 = string.Empty;
        public string ScheduleImage3
        {
            get { return scheduleImage3; }
            set { scheduleImage3 = value; }
        }

        int productPrice = 0;
        /// <summary>
        /// 商品编辑控件中的价格选择模式
        /// </summary>
        public int ProductPrice
        {
            get { return productPrice; }
            set { productPrice = value; }
        }

        string emailSmtp = string.Empty;
        /// <summary>
        /// SMTP 服务器
        /// </summary>
        public string EmailSmtp { get { return emailSmtp; } set { emailSmtp = value; } }

        string emailName = string.Empty;
        public string EmailName { get { return emailName; } set { emailName = value; } }

        string emailPwd = string.Empty;
        public string EmailPwd { get { return emailPwd; } set { emailPwd = value; } }

        bool emailSsl = false;
        public bool EmailSsl { get { return emailSsl; } set { emailSsl = value; } }

        int emailPort = 25;
        public int EmailPort { get { return emailPort; } set { emailPort = value; } }

        string ws = "000001";
        /// <summary>
        /// 报表查询起始时间
        /// </summary>
        public string WorkStart { get { return ws; } set { ws = value; } }

        string we = "235959";
        /// <summary>
        /// 报表查询结束时间
        /// </summary>
        public string WorkEnd { get { return we; } set { we = value; } }

        bool synchronousData = true;
        /// <summary>
        /// 同步刷新用户界面数据
        /// </summary>
        public bool SynchronousData
        {
            get { return synchronousData; }
            set { synchronousData = value; }
        }

        bool gd = false;
        /// <summary>
        /// 查询是否分组返回全部数据
        /// </summary>
        public bool GroupData { get { return gd; } set { gd = value; } }

        uint gdc = 50;
        /// <summary>
        /// 查询返回数据行数
        /// </summary>
        public uint GroupDataCount { get { return gdc; } set { gdc = value; } }

        int alertTime = 10;
        /// <summary>
        /// 预警检测时间
        /// </summary>
        public int AlertTime
        {
            get { return alertTime; }
            set { alertTime = value; }
        }

        string br = "Gray";
        public string BrushString
        {
            get { return br; }
            set { br = value; }
        }
        [XmlIgnore]
        public Brush Brush
        {
            get
            {
                BrushConverter bcc = new BrushConverter();
                if (bcc.CanConvertFrom(typeof(string)))
                    return (Brush)bcc.ConvertFromString(br);
                else
                    return Brushes.Gray;
            }
            set
            {

                BrushConverter bcc = new BrushConverter();
                if (bcc.CanConvertTo(typeof(string)))
                    bcc.ConvertToString(value);
            }
        }

        private List<UserUIparams> m_aceColumns = new List<UserUIparams>();
        /// <summary>
        /// 获取或设置可显示的列与列的排序
        /// </summary>
        [XmlArray("UserUIparams")]
        public List<UserUIparams> DataGridBinding
        {
            get { return m_aceColumns; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                m_aceColumns = value;
            }
        }
    }
}