﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace DoubleH.Utility.Configuration
{
    [Serializable]
    public  class AppConfig
    {
        bool autoLoadTabTip = false;
        /// <summary>
        /// 是否自动加载屏幕手写板
        /// </summary>
        public bool AutoLoadTabTip
        {
            get { return autoLoadTabTip; }
            set { autoLoadTabTip = value; }
        }

        bool forceUpdate = false;
        /// <summary>
        /// 当前更新版本是否强制更新（仅程序升级使用）
        /// </summary>
        public bool ForceUpdate
        {
            get { return forceUpdate; }
            set { forceUpdate = value; }
        }
        bool debug = false;
        /// <summary>
        /// 是否启用调试模式
        /// </summary>
        public bool AppDebug { get { return debug; } set { debug = value; } }

        #region server
        string defaultHost = string.Empty;
        /// <summary>
        /// 客户端地址（如果是双网卡，就利用这个绑定使用那张网卡）
        /// </summary>
        public string DefaultHost { get { return defaultHost; } set { defaultHost = value; } }

        int defaultHostPort = 1984;
        /// <summary>
        /// 客户端端口
        /// </summary>
        public int DefaultHostPort { get { return defaultHostPort; } set { defaultHostPort = value; } }

        int httpPort = 8099;
        /// <summary>
        /// web 服务器端口
        /// </summary>
        public int WebPort { get { return httpPort; } set { httpPort = value; } }

        bool autoWeb = true;
        /// <summary>
        /// (停用)是否自动开启Web服务
        /// </summary>
        public bool AutoWeb
        {
            get { return autoWeb; }
            set { autoWeb = value; }
        }
        #endregion

        #region program

        string serverIP = string.Empty;
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string ServerIP { get { return serverIP; } set { serverIP = value; } }

        int serverPort = 1984;
        /// <summary>
        /// 服务器端口
        /// </summary>
        public int ServerPort { get { return serverPort; } set { serverPort = value; } }

        int reloadDataTime = 60;
        /// <summary>
        /// 间隔多少秒刷新DataGrid数据,小于60为不刷新
        /// </summary>
        public int ReloadDataTime
        {
            get { return reloadDataTime; }
            set { reloadDataTime = value; }
        }

        bool checkUpdate = true;
        /// <summary>
        /// 是否检查更新
        /// </summary>
        public bool CheckUpdate
        {
            get { return checkUpdate; }
            set { checkUpdate = value; }
        }

        string shopNO = "00";
        /// <summary>
        /// 店铺标识
        /// </summary>
        public string ShopNO
        {
            get { return shopNO; }
            set { shopNO = value; }
        }

        int delayTime = 300;
        /// <summary>
        /// 数据库搜索的延迟时间/毫秒
        /// </summary>
        public int DelayTime
        {
            get { return delayTime; }
            set { delayTime = value; }
        }

        int searchThreshold = 2;
        /// <summary>
        /// 搜索的最低字符数量要求
        /// </summary>
        public int SearchThreshold
        {
            get { return searchThreshold; }
            set { searchThreshold = value; }
        }

        string ln2 = string.Empty;
        /// <summary>
        /// 最后POS登录名
        /// </summary>
        public string LastPosName
        {
            get { return ln2; }
            set { ln2 = value; }
        }

        string st = FCNS.Data.DataType.SQLITE.ToString();
        public string DataFlag
        {
            get { return st; }
            set { st = value.Trim(); }
        }

        private ObservableCollection<DataConfig> configItems = new ObservableCollection<DataConfig>();
        /// <summary>
        /// 数据库列表
        /// </summary>
        [XmlArray("DataConfig")]
        public ObservableCollection<DataConfig> DataConfigItems
        {
            get { return configItems; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                configItems = value;
            }
        }
        #endregion

        #region POS
        int posFontSize = 12;
        /// <summary>
        /// POS机触摸屏的商品选择框中字体大小
        /// </summary>
        public int PosProductFontSize
        {
            get {if(posFontSize<8)
                posFontSize = 8;
                
                return posFontSize;
            }
            set {  posFontSize = value; }
        }

        int posProductShowMode = 0;
        /// <summary>
        /// POS机触摸屏的商品选择框的显示模式，1有图没字，2有字没图，其他为默认值
        /// </summary>
        public int PosProductShowMode
        {
            get { return posProductShowMode; }
            set { posProductShowMode = value; }
        }

        int xiaoliang = 0;
        /// <summary>
        /// 触摸屏模式下按销量排行应该大于等于这个值
        /// </summary>
        public int PosProductSCount
        {
            get { return xiaoliang; }
            set { xiaoliang = value; }
        }
        string customProductS = string.Empty;
        /// <summary>
        /// 触摸屏模式下自定义商品的Id列表（用 , 分割）
        /// </summary>
        public string PosCustomProductS
        {
            get { return customProductS; }
            set { customProductS = value; }
        }
        int productSMode = 0;
        /// <summary>
        /// 触摸屏模式下，首页商品的显示模式
        /// </summary>
        public int PosProductSMode
        {
            get { return productSMode; }
            set { productSMode = value; }
        }

        string posLed = "无";
        /// <summary>
        /// pos机顾客屏的com口名称，无 或者 为空 表示不开启
        /// </summary>
        public string PosLed
        {
            get { return posLed; }
            set { posLed = value; }
        }

        bool showCashboxMoney = true;
        /// <summary>
        /// 显示钱箱金额
        /// </summary>
        public bool ShowCashboxMoney
        {
            get { return showCashboxMoney; }
            set { showCashboxMoney = value; }
        }

        double cashboxMoney = 0;
        /// <summary>
        /// 钱箱金额,每次交班登陆系统后重置.
        /// </summary>
        public double PosCashboxMoney
        {
            get { return cashboxMoney; }
            set { cashboxMoney = Math.Round(value, FCNS.Data.Table.SysConfig.SysConfigParams.DecimalPlaces); }
        }

        string jiaoBan = string.Empty;
        /// <summary>
        /// 强制交班的时间
        /// </summary>
        public string PosJiaoBan
        {
            get { return jiaoBan; }
            set { jiaoBan = value; }
        }

        string posType = string.Empty;
        /// <summary>
        /// pos 机标识，属于什么类型
        /// </summary>
        public string PosFlag
        {
            get { return posType; }
            set { posType = value; }
        }

        string posMode = string.Empty;
        /// <summary>
        /// 收款界面模板名称
        /// </summary>
        public string PosTempleteName
        {
            get { return posMode; }
            set { posMode = value; }
        }

        string backgroundColor = "Black";
        /// <summary>
        /// UI 背景色
        /// </summary>
        public string PosBackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; }
        }

        string foregroundColor = "White";
        /// <summary>
        /// UI 前景色
        /// </summary>
        public string PosForegroundColor
        {
            get { return foregroundColor; }
            set { foregroundColor = value; }
        }

        bool isPrint = false;
        /// <summary>
        /// 是否打印小票
        /// </summary>
        public bool PosIsPrint { get { return isPrint; } set { isPrint = value; } }

        bool isPrintJiaoBan = false;
        /// <summary>
        /// 打印交班小票
        /// </summary>
        public bool PosIsPrintJiaoBan { get { return isPrintJiaoBan; } set { isPrintJiaoBan = value; } }
        
        string posNO = string.Empty;
        /// <summary>
        /// pos 机器号
        /// </summary>
        public string PosNO { get { return posNO; } set { posNO = value; } }

        bool singleMode = true;
        /// <summary>
        ///  pos 允许单机销售
        /// </summary>
        public bool SingleMode { get { return singleMode; } set { singleMode = value; } }

        bool fullScreen = false;
        /// <summary>
        ///  pos 全屏模式
        /// </summary>
        public bool PosFullScreen { get { return fullScreen; } set { fullScreen = value; } }

       

        //bool posWindowMax = true;
        //public bool PosWindowMax { get { return posWindowMax; } set { posWindowMax = value; } }

        double posLeft = 0;
        public double PosLeft { get { return posLeft; } set { posLeft = value; } }

        double posTop = 0;
        public double PosTop { get { return posTop; } set { posTop = value; } }

        double posWidth = 800;
        public double PosWidth { get { return posWidth; } set { posWidth = value; } }

        double posHeight = 600;
        public double PosHeight { get { return posHeight; } set { posHeight = value; } }
        #endregion

        #region Print
        private List<PrintConfig> printColumns = new List<PrintConfig>();
        /// <summary>
        /// 打印参数的配置列表
        /// </summary>
        [XmlArray("PrintConfig")]
        public List<PrintConfig> PrintConfigItems
        {
            get { return printColumns; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                printColumns = value;
            }
        }
        #endregion
    }

    [Serializable]
    public class PrintConfig
    {
        string tableText = string.Empty;
        public string TableText
        {
            get { return tableText; }
            set { tableText = value; }
        }

        string fileName = string.Empty;
        /// <summary>
        /// 打印模板名
        /// </summary>
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        int row = 0;//这个值不能更改
        /// <summary>
        /// 分页行数
        /// 如果为0则连续打印不分页
        /// </summary>
        public int Row
        {
            get { return row; }
            set { row = value; }
        }

        bool isTaoDa = false;
        /// <summary>
        /// 是否套打
        /// </summary>
        public bool IsTaoDa
        {
            get { return isTaoDa; }
            set { isTaoDa = value; }
        }

        bool isPage =false;
        /// <summary>
        /// 是否分页
        /// </summary>
        public bool IsPage
        {
            get { return isPage; }
            set { isPage = value; }
        }

        double left = 0;
        /// <summary>
        /// 左边距
        /// </summary>
        public double Left { get { return left; } set { left = value; } }

        double right = 0;
        /// <summary>
        /// 右边距
        /// </summary>
        public double Right { get { return right; } set { right = value; } }

        double top = 0;
        /// <summary>
        /// 上边距
        /// </summary>
        public double Top { get { return top; } set { top = value; } }

        double bottom = 0;
        /// <summary>
        /// 下边距
        /// </summary>
        public double Bottom { get { return bottom; } set { bottom = value; } }
    }

    [Serializable]
    public class DataConfig
    {
        int timeout = 1;
        public int TimeOut
        {
            get
            {
                if (timeout <= 0)//小于等于0都会验证为无法连接数据库
                    timeout = 1;

                return timeout;
            }
            set { timeout = value; }
        }

        string flag = string.Empty;
        public string Flag
        {
            get { return flag; }
            set { flag = value; }
        }

        string st = FCNS.Data.DataType.SQLITE.ToString();
        public string DataType
        {
            get { return st; }
            set { st = value.Trim(); }
        }

        string na = FCNS.Data.DbDefine.dbFile;
        public string DataName
        {
            get { return na; }
            set { na = value.Trim(); }
        }

        string mi = string.Empty;
        public string DataAddress
        {
            get { return mi; }
            set { mi = value.Trim(); }
        }

        string mu = string.Empty;
        public string DataUser
        {
            get { return mu; }
            set { mu = value.Trim(); }
        }

        string mp = string.Empty;
        public string DataPassword
        {
            get { return mp; }
            set { mp = value; }
        }

        string port = "1433";
        public string Port { get { return port; } set { port = value; } }

        string ln = string.Empty;
        /// <summary>
        /// 最后登录的用户名
        /// </summary>
        public string LastName
        {
            get { return ln; }
            set { ln = value.Trim(); }
        }
    }
}
