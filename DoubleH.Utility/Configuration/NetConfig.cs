﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoubleH.Utility.Configuration
{
    [Serializable]
    public class NetConfig
    {
        /// <summary>
        /// 飞鸽客户端用到的设置
        /// </summary>
        public FSLib.IPMessager.Entity.Config IPMClientConfig { get; set; }

        #region 功能设置

        /// <summary>
        /// 主机功能设置
        /// </summary>
        public HostInfoConfig HostInfo { get; set; }

        /// <summary>
        /// 客户端功能设置
        /// </summary>
        public ClientFunctionConfig FunctionConfig { get; set; }

        private SoundConfig _sound;
        /// <summary>
        /// 声音设置
        /// </summary>
        public SoundConfig Sound
        {
            get
            {
                if (_sound == null) _sound = new SoundConfig();

                return _sound;
            }
            set
            {
                _sound = value;
            }
        }

        #endregion

        #region 对象操作

        /// <summary>
        /// 保存配置更改
        /// </summary>
        public void Save()
        {
            Core.ProfileManager.SaveConfig(this);
        }


        #endregion
    }
}
