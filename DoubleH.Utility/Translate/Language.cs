﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoubleH.Utility.Translate
{
   public class Language
    {
       string _还车里程数_必须大于_出车里程数 = "'还车里程数' 必须大于 '出车里程数'";
       public string 还车里程数_必须大于_出车里程数
        {
            get { return _还车里程数_必须大于_出车里程数; }
        }
       string _客户名称 = "客户名称:";
       public string 客户名称
        {
            get { return _客户名称; }
        }

        string _发货仓库 = "发货仓库:";
        public string 发货仓库
        {
            get { return _发货仓库; }
        }

        string _发货地址 = "发货地址:";
        public string 发货地址
        {
            get { return _发货地址; }
        }

        string _供应商 = "供 应 商:";
        public string 供应商
        {
            get { return _供应商; }
        }

        string _收货仓库 = "收货仓库:";
        public string 收货仓库
        {
            get { return _收货仓库; }
        }

        string _收货地址= "收货地址:";
        public string 收货地址
        {
            get { return _收货地址; }
        }

       string _存在数量为0的行将自动移除 = "存在数量为 0 的行将自动移除,是否返回编辑?";
       public string 存在数量为0的行将自动移除
        {
            get { return _存在数量为0的行将自动移除; }
        }

       string _确定要删除此单据吗 = "确定要删除此单据吗?";
       public string 确定要删除此单据吗
        {
            get { return _确定要删除此单据吗; }
        }

       string _没有上一条记录了 = "没有上一条记录了";
        public string 没有上一条记录了
        {
            get { return _没有上一条记录了; }
        }

        string _没有下一条记录了 = "没有下一条记录了";
        public string 没有下一条记录了
        {
            get { return _没有下一条记录了; }
        }

       string _仓库名称不能为空 = "仓库名称不能为空";
       public  string 仓库名称不能为空
       {
           get { return _仓库名称不能为空; }
       }
    }
}
