﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoubleH.Utility
{
    /// <summary>
    /// SqlWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SqlWindow : Window
    {
        public SqlWindow()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            textBoxSql.Focus();
        }

        private void InitEvent()
        {
            buttonRun.Click += (ss, ee) =>
            {
                if (textBoxSql.Text.Trim().StartsWith("select", true, null))
                    dataGrid1.ItemsSource = FCNS.Data.SQLdata.GetDataTable(textBoxSql.Text).DefaultView;
                else
                {
                    FCNS.Data.SQLdata.ExecuteScalar(textBoxSql.Text);
                    DoubleH.Utility.MessageWindow.Show("执行成功");
                }
            };
        }
    }
}