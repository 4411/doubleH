﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using FCNS.Data;
using System.Diagnostics;

namespace DoubleH.Utility
{

    /// <summary>
    /// PrintConfig.xaml 的交互逻辑
    /// </summary>
    public partial class PrintConfig : Window
    {
        public PrintConfig()
        {
            InitializeComponent();

            InitEvent();
        }

        DoubleH.Utility.Configuration.PrintConfig config = null;
        PrintTempleteClass selected = null;

        public void Init(DoubleH.Utility.Configuration.PrintConfig obj)
        {
            config = obj;
            Debug.Assert(config != null);

            treeViewFolder.ItemsSource = SearchPrintTemplete(FCNS.Data.DbDefine.printDir);
            checkBoxPage.IsChecked = config.IsPage;
            checkBoxTaoDa.IsChecked = config.IsTaoDa;
            integerUpDownRow.Value = config.Row;
            integerUpDownRow.IsEnabled = checkBoxPage.IsEnabled;

            doubleUpDownLeft.Value = config.Left;
            doubleUpDownRight.Value = config.Right;
            doubleUpDownTop.Value = config.Top;
            doubleUpDownBottom.Value = config.Bottom;
        }

        private List<PrintTempleteClass> SearchPrintTemplete(string dirName)
        {
            List<PrintTempleteClass> all = new List<PrintTempleteClass>();
            foreach (string dir in Directory.GetDirectories(dirName))
            {
                if (dir.EndsWith("\\js"))
                    continue;

                PrintTempleteClass tc = new PrintTempleteClass();
                tc.DirName = System.IO.Path.GetFileName(dir);
                tc.AllFile = Directory.GetFiles(dir, "*.html", SearchOption.TopDirectoryOnly);
                for (int i = 0; i < tc.AllFile.Length; i++)
                    tc.AllFile[i] = System.IO.Path.GetFileName(tc.AllFile[i]);

                tc.SubDir = SearchPrintTemplete(dir);
                all.Add(tc);
            }
            return all;
        }

        private void InitEvent()
        {
            checkBoxPage.Click += (ss, ee) => integerUpDownRow.IsEnabled = checkBoxPage.IsEnabled;
            treeViewFolder.MouseUp += (ss, ee) => SelectTemplete();
            buttonOk.Click += (ss, ee) => Save();
        }

        private void SelectTemplete()
        {
            if (treeViewFolder.SelectedItems.Count == 0)
                return;

            selected = treeViewFolder.SelectedItems[0] as PrintTempleteClass;
            listBoxFile.ItemsSource = selected.AllFile;
        }

        private void ChangedMode()
        {
            doubleUpDownLeft.Value = config.Left;
            doubleUpDownTop.Value = config.Top;
            doubleUpDownRight.Value = config.Right;
            doubleUpDownBottom.Value = config.Bottom;
        }

        private void Save()
        {
            if (listBoxFile.SelectedItem != null && selected != null)
                config.FileName = selected.DirName + "\\" + listBoxFile.SelectedItem.ToString();

            config.IsPage = checkBoxPage.IsChecked.Value;
            config.IsTaoDa = checkBoxTaoDa.IsChecked.Value;
            config.Row = integerUpDownRow.Value.Value;

            config.Left = doubleUpDownLeft.Value.Value;
            config.Top = doubleUpDownTop.Value.Value;
            config.Right = doubleUpDownRight.Value.Value;
            config.Bottom = doubleUpDownBottom.Value.Value;
            this.Close();
        }
    }

    public class PrintTempleteClass
    {
        public string DirName { get; set; }
        public List<PrintTempleteClass> SubDir { get; set; }
        public string[] AllFile { get; set; }
        public override string ToString()
        {
            return DirName;
        }
    }

}