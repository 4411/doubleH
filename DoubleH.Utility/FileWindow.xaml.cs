﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace DoubleH.Utility
{
    /// <summary>
    /// OpenFileWindow.xaml 的交互逻辑
    /// </summary>
    public partial class FileWindow
    {
        public FileWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        public enum EnumFileType
        {
            NULL,
            商品控件的导入导出
        }

        public string FileName
        {
            get { return textBoxFile.Text; }
        }

        bool canel = true;
        public bool Canel
        {
            get { return canel; }
            set { canel = value; }
        }

        EnumFileType fileType = EnumFileType.NULL;
        public EnumFileType FileType { set { fileType = value; } }

        bool isOpen = true;
        /// <summary>
        /// 文件打开还是保存
        /// </summary>
        public bool IsOpen
        {
            get { return isOpen; }
            set
            {
                isOpen = value;
                this.Title = isOpen ? "导入数据" : "导出数据";
            }
        }

        public void Init() { }

        private void InitEvent()
        {
            buttonBrowser.Click += (ss, ee) => SelectFile();
            buttonSave.Click += (ss, ee) => SaveFile();
            buttonHelp.Click += (ss, ee) =>  SaveHelpFile(); 
        }

        private void SelectFile()
        {
            if (IsOpen)
                Open();
            else
                Save();
        }

        private void SaveFile()
        {
            if (IsOpen)
            {
                if (!File.Exists(textBoxFile.Text))
                {
                    MessageWindow.Show("文件不存在");
                    return;
                }
            }
            canel = false;
            this.Close();
        }

        private void Open()
        {
            using (System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog())
            {
                switch (fileType)
                {
                    case EnumFileType.商品控件的导入导出:
                        ofd.Filter = "(*.txt)|*.txt";
                        break;

                    default:
                        ofd.Filter = "(*.xls)|*.xls";
                        break;
                }

                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    textBoxFile.Text = ofd.FileName;
            }
        }

        private void Save()
        {
            using (System.Windows.Forms.SaveFileDialog ofd = new System.Windows.Forms.SaveFileDialog())
            {
                switch (fileType)
                {
                    case EnumFileType.商品控件的导入导出:
                        ofd.Filter = "(*.txt)|*.txt";
                        break;

                    default:
                        ofd.Filter = "(*.xls)|*.xls";
                        break;
                }

                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    textBoxFile.Text = ofd.FileName;
            }
        }

        private void SaveHelpFile()
        {
            if (!Directory.Exists(DbDefine.helpDir))
                Directory.CreateDirectory(DbDefine.helpDir);

            switch (fileType)
            {
                case EnumFileType.商品控件的导入导出:
                    string file = DbDefine.helpDir + EnumFileType.商品控件的导入导出.ToString() + ".txt";
                    File.WriteAllText(file, "商品Id,编码,条形码,价格,数量,备注\r\n" + "优先使用'商品Id,如果Id<0或为空则使用'编码',如果'编码'也为空则使用'条形码'", Encoding.UTF8);
                    System.Diagnostics.Process.Start(file);
                    break;
            }
        }
    }
}
