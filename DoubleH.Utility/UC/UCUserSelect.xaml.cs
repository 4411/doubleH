﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// UCuserSelect.xaml 的交互逻辑
    /// </summary>
    public partial class UCUserSelect : ComboBox
    {
        public UCUserSelect()
        {
            InitializeComponent();

            this.DisplayMemberPath = "Name";
            this.SelectedValuePath = "Id";
        }

        public void Init(Table.UserS.EnumFlag flag)
        {
            this.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, flag);
        }

        public void Init(params Table.UserS.EnumFlag[] flag)
        {
            this.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, flag);
        }
        /// <summary>
        /// 初始化指定客商的联系人列表
        /// </summary>
        /// <param name="corSId"></param>
        public void Init(Int64 corSId, params Table.UserS.EnumEnable[] enable)
        {
            this.ItemsSource = Table.UserS.GetListForCorS(corSId, enable);
        }

        public Table.UserS SelectedObject
        {
            get
            {
                if (SelectedItem == null)
                    return null;
                else
                    return (Table.UserS)SelectedItem;
            }
            set
            {
                SelectedItem = value;
            }
        }

        public Int64 SelectedObjectId
        {
            get
            {
                if (SelectedObject == null)
                    return -1;
                else
                    return SelectedObject.Id;
            }
            set
            {
                SelectedValue = value;
            }
        }

        public string SelectedObjectPhone
        {
            get
            {
                if (SelectedObject == null)
                    return string.Empty;
                else
                    return SelectedObject.Phone;
            }
        }
    }
}
