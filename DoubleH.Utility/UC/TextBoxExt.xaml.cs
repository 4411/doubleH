﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Diagnostics;
using DoubleH.Utility.Configuration;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// Interaction logic for TextBox.xaml
    /// </summary>    
    public partial class TextBoxExt : Canvas
    {
        public delegate void Selected(DataRow row);
        public event Selected SelectedRow;

        private VisualCollection controls;
        private TextBox textBox;
        private ComboBox comboBox;
        //private ObservableCollection<DataRow> autoCompletionList;
        private ObservableCollection<DataRow> filterRows = new ObservableCollection<DataRow>();
        private System.Timers.Timer keypressTimer;
        private delegate void TextChangedCallback();
        private bool insertText;
        private int delayTime;
        private int searchThreshold;
        string sqlString = string.Empty;
        string[] searchColumn = null;
        string[] displayColumn = null;
        string sql = string.Empty;

        public TextBoxExt()
        {
            controls = new VisualCollection(this);
            InitializeComponent();

            InitUI();
            InitVar();
        }

        private void InitUI()
        {
            comboBox = new ComboBox();
            comboBox.IsSynchronizedWithCurrentItem = true;
            comboBox.IsTabStop = false;
            comboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);
            comboBox.MinWidth = 10;

            textBox = new TextBox();
            textBox.MinWidth = 10;
            textBox.TextChanged += new TextChangedEventHandler(textBox_TextChanged);
            textBox.VerticalContentAlignment = VerticalAlignment.Center;

            controls.Add(comboBox);
            controls.Add(textBox);
        }

        private void InitVar()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            keypressTimer = new System.Timers.Timer();
            keypressTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);

            searchThreshold = DoubleHConfig.AppConfig.SearchThreshold;        // default threshold to 2 char
            delayTime = DoubleHConfig.AppConfig.DelayTime;
        }

        public string Text
        {
            get { return textBox.Text; }
            set
            {
                insertText = true;
                textBox.Text = value;
            }
        }

        public void Init(string tableName, string[] searchColumn, string[] displayColumn, string selectColumn)
        {
            Debug.Assert(!string.IsNullOrEmpty(tableName));
            Debug.Assert(displayColumn != null);
            Debug.Assert(searchColumn!= null);

            this.searchColumn = searchColumn;
            this.displayColumn = displayColumn;
            comboBox.DisplayMemberPath = "[" + displayColumn[0] + "]";

            StringBuilder sb = new StringBuilder("select ");
            foreach (string str in displayColumn)
                sb.Append(str + ",");

            sb.Remove(sb.Length - 1, 1);
            sb.Append(" from " + tableName + " where ");
            sqlString = sb.ToString();
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (null != comboBox.SelectedItem)
            {
                insertText = true;
                DataRow row = (DataRow)comboBox.SelectedItem;
                textBox.Text = row[displayColumn[0]].ToString();

                if (SelectedRow != null)
                    SelectedRow(row);
            }
        }

        private void TextChanged()
        {
            try
            {
                filterRows.Clear();
                if (textBox.Text.Length >= searchThreshold)
                {
                    string str2="(";
                    foreach (string str in searchColumn)
                        str2 += (str + " like '%"+Text+"%' or ");

                    str2=str2.Remove(str2.Length-3);
                    str2 += ")";
                    foreach (DataRow entry in FCNS.Data.SQLdata.GetDataRows(sqlString+str2))
                        filterRows.Add(entry);

                    //comboBox.ItemsSource = filterRows.Distinct<DataRow>(new NormalUtility.Compare<DataRow>((x,y)=>x[0].ToString().Trim()==y[0].ToString().Trim()));
                    comboBox.Items.Clear();
                    foreach (DataRow r in filterRows.Distinct<DataRow>(new NormalUtility.Compare<DataRow>((x, y) => x[0].ToString().Trim() == y[0].ToString().Trim())))
                        comboBox.Items.Add(r);

                    comboBox.IsDropDownOpen = comboBox.HasItems;
                    //comboBox.SelectedItem = null;
                }
                else
                    comboBox.IsDropDownOpen = false;
            }
            catch { }
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                new TextChangedCallback(this.TextChanged));
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // text was not typed, do nothing and consume the flag
            if (insertText == true) insertText = false;

            // if the delay time is set, delay handling of text changed
            else
            {
                if (delayTime > 0)
                {
                    keypressTimer.Interval = delayTime;
                    keypressTimer.Start();
                }
                else TextChanged();
            }
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            textBox.Arrange(new Rect(arrangeSize));
            comboBox.Arrange(new Rect(arrangeSize));
            return base.ArrangeOverride(arrangeSize);
        }

        protected override Visual GetVisualChild(int index)
        {
            return controls[index];
        }

        protected override int VisualChildrenCount
        {
            get { return controls.Count; }
        }
    }
}