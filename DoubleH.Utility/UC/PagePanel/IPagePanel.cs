﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Collections;
using System.ComponentModel;

namespace DoubleH.Utility.UC.PagePanel
{
    public interface IPagePanel
    {
        object SelectedItem { get; }

        IEnumerable SelectedItems { get; }

        object SelectedGroupItem { get; }

        Brush BackgroundBrush { set; }

        Brush ForegroundBrush { set; }

        void InitHomeData(IList obj);

        void InitData(IList groups, IList items);

        void Init(UserUIparams userUIparams, PagePanel.PageListView.EnumModeTemplete bodyTempleteName);

        event Utility.UC.UCPagePanel.dDoubleClickItem ItemDoubleClick;

        event Utility.UC.UCPagePanel.dClickItem ItemClick;

    }
}
