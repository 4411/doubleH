﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// POS 端顾客屏
    /// </summary>
   public class PosLed
    {
        private int _baudRate = 2400;
        private string _portName = "COM1";
        private SerialPort serialPort1 = null;
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRate
        {
            get { return _baudRate; }
            set { _baudRate = value; }
        }
        /// <summary>
        /// 端口
        /// </summary>
        public string PortName
        {
            get { return _portName; }
            set { _portName = value; }
        }

        /// <summary>
        /// 总计
        /// </summary>
        /// <param name="value">The value.</param>
        public void Sum(string value)
        {
            //clear();            
            newData(value);
            总计();
        }
        /// <summary>
        /// 单价
        /// </summary>
        /// <param name="value">The value.</param>
        public void UnitPrice(string value)
        {
            //clear();
            newData(value);

            单价();
        }
        /// <summary>
        /// 收款
        /// </summary>
        /// <param name="value">The value.</param>
        public void Get(string value)
        {
            //clear();
            newData(value);

            收款();
        }
        /// <summary>
        /// 找零
        /// </summary>
        /// <param name="value">The value.</param>
        public void OddChange(string value)
        {
            //clear();
            newData(value);
            找零();
        }
        /// <summary>
        ///全暗.是找零 全价 收款 这几个灯
        /// </summary>
        public void AllBlack()
        {
            // serialPort1.WriteLine(@"s0");
            serialPort1.Write(@"s0");
        }
        /// <summary>
        /// 清屏.
        /// </summary>
        public void Clear()
        {
            //serialPort1.WriteLine("\f");
            serialPort1.Write(((char)(12)).ToString());
        }
        private void 单价()
        {
            //serialPort1.WriteLine(@"s1");//其实有数据的那个空格
            serialPort1.Write(@"s1");//其实有数据的那个空格
        }
        private void 总计()
        {
            serialPort1.WriteLine(@"s2");
        }
        private void 收款()
        {
            serialPort1.WriteLine(@"s3");
        }
        private void 找零()
        {
            serialPort1.WriteLine(@"s4");
        }
        private void newData(string data)
        {
            //serialPort1.WriteLine(@"QA" + data);
            serialPort1.Write(@"QA" + data + ((char)(13)).ToString());
        }
        /// <summary>
        /// 打开串口,可能抛出异常
        /// </summary>
        public void Open()
        {
            try
            {
                if (serialPort1 == null)
                {
                    serialPort1 = new SerialPort();
                    serialPort1.PortName = _portName;
                    serialPort1.BaudRate = _baudRate;
                }
                if (!serialPort1.IsOpen)
                {
                    serialPort1.Open();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("打开串口失败!错误信息为:" + ex.Message);
            }

        }
        /// <summary>
        /// 关闭串口
        /// </summary>
        public void Close()
        {
            if (serialPort1 != null && serialPort1.IsOpen)
                serialPort1.Close();
        }
    }
}
