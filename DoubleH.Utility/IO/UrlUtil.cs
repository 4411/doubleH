using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace DoubleH.Utility.IO
{
	/// <summary>
	/// 路径操作函数
	/// </summary>
	public static class UrlUtil
	{
        internal const int MAX_PATH = 260;

		private static readonly char[] m_vDirSeps = new char[] { '\\', '/',
			Path.DirectorySeparatorChar };

		/// <summary>
		/// 获取文件目录，返回值包含了分隔符。例如：<c>C:\\My Documents\\My File.kdb</c>  <paramref name="strFile" />
		/// 会生成 <c>C:\\My Documents\\</c>
		/// </summary>
		/// <param name="strFile">文件的完整路径</param>
		/// <param name="bAppendTerminatingChar">是否在路径结尾附上分隔符？</param>
		/// <param name="bEnsureValidDirSpec">如果为 true, 则返回值必为正确路径
        /// （例如： <c>X:\\</c>会替代<c>X:</c>).最主要的是如果返回值是直接传递给 API 函数， 
        /// 则<paramref name="bAppendTerminatingChar" />一定要设置为 true</param>
        /// <returns>假如参数为 null，会返回空字符 "".</returns>
		public static string GetFileDirectory(string strFile, bool bAppendTerminatingChar,
			bool bEnsureValidDirSpec)
		{
			Debug.Assert(strFile != null);
			if(strFile == null) throw new ArgumentNullException("strFile");

			int nLastSep = strFile.LastIndexOfAny(m_vDirSeps);
			if(nLastSep < 0) return strFile; // None

			if(bEnsureValidDirSpec && (nLastSep == 2) && (strFile[1] == ':') &&
				(strFile[2] == '\\')) // Length >= 3 and Windows root directory
				bAppendTerminatingChar = true;

			if(!bAppendTerminatingChar) return strFile.Substring(0, nLastSep);
			return EnsureTerminatingSeparator(strFile.Substring(0, nLastSep), false);
		}

		/// <summary>
		/// 获取文件名,例如:<paramref name="strPath" /> 是 <c>C:\\My Documents\\My File.kdb</c>
		///  则返回值为 <c>My File.kdb</c>.
		/// </summary>
		/// <param name="strPath">文件路径</param>
		/// <returns>string</returns>
		public static string GetFileName(string strPath)
		{
			Debug.Assert(strPath != null); if(strPath == null) throw new ArgumentNullException("strPath");

			int nLastSep = strPath.LastIndexOfAny(m_vDirSeps);

			if(nLastSep < 0) return strPath;
			if(nLastSep >= (strPath.Length - 1)) return string.Empty;

			return strPath.Substring(nLastSep + 1);
		}

		/// <summary>
		/// 移除文件的扩展名
		/// </summary>
		/// <param name="strPath">文件路径</param>
		/// <returns>string</returns>
		public static string StripExtension(string strPath)
		{
			Debug.Assert(strPath != null); if(strPath == null) throw new ArgumentNullException("strPath");

			int nLastDirSep = strPath.LastIndexOfAny(m_vDirSeps);
			int nLastExtDot = strPath.LastIndexOf('.');

			if(nLastExtDot <= nLastDirSep) return strPath;

			return strPath.Substring(0, nLastExtDot);
		}

		/// <summary>
		/// Get the extension of a file.
		/// </summary>
		/// <param name="strPath">Full path of a file with extension.</param>
		/// <returns>Extension without prepending dot.</returns>
		public static string GetExtension(string strPath)
		{
			Debug.Assert(strPath != null); if(strPath == null) throw new ArgumentNullException("strPath");

			int nLastDirSep = strPath.LastIndexOfAny(m_vDirSeps);
			int nLastExtDot = strPath.LastIndexOf('.');

			if(nLastExtDot <= nLastDirSep) return string.Empty;
			if(nLastExtDot == (strPath.Length - 1)) return string.Empty;

			return strPath.Substring(nLastExtDot + 1);
		}

		/// <summary>
		/// 确认路径结尾必需有分隔符
		/// </summary>
		/// <param name="strPath">文件路径</param>
		/// <param name="bUrl">如果为 true, 将添加网址分隔符 / 到路径结尾，
        /// 如果为 false, 将添加系统分隔符到路径结尾</param>
		/// <returns>返回带分隔符结尾的文件路径</returns>
		public static string EnsureTerminatingSeparator(string strPath, bool bUrl)
		{
			Debug.Assert(strPath != null); if(strPath == null) throw new ArgumentNullException("strPath");

			int nLength = strPath.Length;
			if(nLength <= 0) return string.Empty;

			char chLast = strPath[nLength - 1];

			for(int i = 0; i < m_vDirSeps.Length; ++i)
			{
				if(chLast == m_vDirSeps[i]) return strPath;
			}

			if(bUrl) return (strPath + '/');
			return (strPath + Path.DirectorySeparatorChar);
		}

		/* /// <summary>
		/// File access mode enumeration. Used by the <c>FileAccessible</c>
		/// method.
		/// </summary>
		public enum FileAccessMode
		{
			/// <summary>
			/// Opening a file in read mode. The specified file must exist.
			/// </summary>
			Read = 0,

			/// <summary>
			/// Opening a file in create mode. If the file exists already, it
			/// will be overwritten. If it doesn't exist, it will be created.
			/// The return value is <c>true</c>, if data can be written to the
			/// file.
			/// </summary>
			Create
		} */

		/* /// <summary>
		/// Test if a specified path is accessible, either in read or write mode.
		/// </summary>
		/// <param name="strFilePath">Path to test.</param>
		/// <param name="fMode">Requested file access mode.</param>
		/// <returns>Returns <c>true</c> if the specified path is accessible in
		/// the requested mode, otherwise the return value is <c>false</c>.</returns>
		public static bool FileAccessible(string strFilePath, FileAccessMode fMode)
		{
			Debug.Assert(strFilePath != null);
			if(strFilePath == null) throw new ArgumentNullException("strFilePath");

			if(fMode == FileAccessMode.Read)
			{
				FileStream fs;

				try { fs = File.OpenRead(strFilePath); }
				catch(Exception) { return false; }
				if(fs == null) return false;

				fs.Close();
				return true;
			}
			else if(fMode == FileAccessMode.Create)
			{
				FileStream fs;

				try { fs = File.Create(strFilePath); }
				catch(Exception) { return false; }
				if(fs == null) return false;

				fs.Close();
				return true;
			}

			return false;
		} */

		public static string GetQuotedAppPath(string strPath)
		{
			int nFirst = strPath.IndexOf('\"');
			int nSecond = strPath.IndexOf('\"', nFirst + 1);

			if((nFirst >= 0) && (nSecond >= 0))
				return strPath.Substring(nFirst + 1, nSecond - nFirst - 1);

			return strPath;
		}
        /// <summary>
        /// 转换 URL 成路径形式，例如：file:///C:\ 转换成 C:\
        /// </summary>
        /// <param name="strUrl">URL 地址</param>
        /// <returns>文件路径</returns>
		public static string FileUrlToPath(string strUrl)
		{
			Debug.Assert(strUrl != null);
            if (strUrl == null)
            {
                throw new ArgumentNullException("strUrl");
            }
			string str = strUrl;
            if (str.ToLower().StartsWith(@"file:///"))
            {
                str = str.Substring(8, str.Length - 8);
            }
			str = str.Replace('/', Path.DirectorySeparatorChar);

			return str;
		}
        //del
//        /// <summary>
//        /// 设置文件为不隐藏属性
//        /// </summary>
//        /// <param name="strFile"></param>
//        /// <returns></returns>
//        public static bool UnhideFile(string strFile)
//        {
//#if KeePassLibSD
//            return false;
//#else
//            if(strFile == null) throw new ArgumentNullException("strFile");

//            try
//            {
//                FileAttributes fa = File.GetAttributes(strFile);
//                if((long)(fa & FileAttributes.Hidden) == 0) return false;

//                return HideFile(strFile, false);
//            }
//            catch(Exception) { }

//            return false;
//#endif
//        }
        /// <summary>
        /// 设置文件的隐藏属性
        /// </summary>
        /// <param name="strFile">文件</param>
        /// <param name="bHide">true 为隐藏</param>
        /// <returns>bool</returns>
		public static bool HideFile(string strFile, bool bHide)
		{
#if KeePassLibSD
			return false;
#else
			if(strFile == null) throw new ArgumentNullException("strFile");

			try
			{
				FileAttributes fa = File.GetAttributes(strFile);

				if(bHide) fa = ((fa & ~FileAttributes.Normal) | FileAttributes.Hidden);
				else 
				{
					fa &= ~FileAttributes.Hidden;
					if((long)fa == 0) fa |= FileAttributes.Normal;
				}

				File.SetAttributes(strFile, fa);
				return true;
			}
			catch(Exception) { }

			return false;
#endif
		}
        /// <summary>
        /// 合并为绝对路径
        /// </summary>
        /// <param name="strBaseFile">目录</param>
        /// <param name="strTargetFile">文件名</param>
        /// <returns>string</returns>
		public static string MakeAbsolutePath(string strBaseFile, string strTargetFile)
		{
			if(strBaseFile == null) throw new ArgumentNullException("strBasePath");
			if(strTargetFile == null) throw new ArgumentNullException("strTargetPath");
			if(strBaseFile.Length == 0) return strTargetFile;
			if(strTargetFile.Length == 0) return string.Empty;

			if(IsAbsolutePath(strTargetFile)) return strTargetFile;

			string strBaseDir = GetFileDirectory(strBaseFile, true, false);
			return GetShortestAbsolutePath(strBaseDir + strTargetFile);
		}
        /// <summary>
        /// 获取文件路径是否为绝对路径
        /// </summary>
        /// <param name="strPath">文件路径</param>
        /// <returns>bool</returns>
		public static bool IsAbsolutePath(string strPath)
		{
			if(strPath == null) throw new ArgumentNullException("strPath");
			if(strPath.Length == 0) return false;

			if(IsUncPath(strPath)) return true;

			try { return Path.IsPathRooted(strPath); }
			catch(Exception) { Debug.Assert(false); }

			return true;
		}

		public static string GetShortestAbsolutePath(string strPath)
		{
			if(strPath == null) throw new ArgumentNullException("strPath");
			if(strPath.Length == 0) return string.Empty;

			// Path.GetFullPath is incompatible with UNC paths traversing over
			// different server shares (which are created by PathRelativePathTo);
			// we need to build the absolute path on our own...
			if(IsUncPath(strPath))
			{
				char chSep = strPath[0];
				Debug.Assert(Array.IndexOf<char>(m_vDirSeps, chSep) >= 0);

				List<string> l = new List<string>();
//#if !KeePassLibSD
				string[] v = strPath.Split(m_vDirSeps, StringSplitOptions.None);
//#else
//                string[] v = strPath.Split(m_vDirSeps);
//#endif
				Debug.Assert((v.Length >= 3) && (v[0].Length == 0) &&
					(v[1].Length == 0));

				foreach(string strPart in v)
				{
					if(strPart.Equals(".")) continue;
					else if(strPart.Equals(".."))
					{
						if(l.Count > 0) l.RemoveAt(l.Count - 1);
						else { Debug.Assert(false); }
					}
					else l.Add(strPart); // Do not ignore zero length parts
				}

				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < l.Count; ++i)
				{
					// Don't test length of sb, might be 0 due to initial UNC seps
					if(i > 0) sb.Append(chSep);

					sb.Append(l[i]);
				}

				return sb.ToString();
			}

			string str;
			try { str = Path.GetFullPath(strPath); }
			catch(Exception) { Debug.Assert(false); return strPath; }

			Debug.Assert(str.IndexOf("\\..\\") < 0);
			foreach(char ch in m_vDirSeps)
			{
				string strSep = new string(ch, 1);
				str = str.Replace(strSep + "." + strSep, strSep);
			}

			return str;
		}

		public static int GetUrlLength(string strText, int nOffset)
		{
			if(strText == null) throw new ArgumentNullException("strText");
			if(nOffset > strText.Length) throw new ArgumentException(); // Not >= (0 len)

			int iPosition = nOffset, nLength = 0, nStrLen = strText.Length;

			while(iPosition < nStrLen)
			{
				char ch = strText[iPosition];
				++iPosition;

				if((ch == ' ') || (ch == '\t') || (ch == '\r') || (ch == '\n'))
					break;

				++nLength;
			}

			return nLength;
		}

        /// <summary>
        /// 转换文件路径的分隔符为当前操作系统的分隔符
        /// </summary>
        /// <param name="strPath"></param>
        /// <returns></returns>
		public static string ConvertSeparators(string strPath)
		{
			return ConvertSeparators(strPath, Path.DirectorySeparatorChar);
		}
        /// <summary>
        /// 转换文件路径的分隔符为当前操作系统的分隔符
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="chSeparator"></param>
        /// <returns></returns>
		public static string ConvertSeparators(string strPath, char chSeparator)
		{
			if(string.IsNullOrEmpty(strPath)) return string.Empty;

			strPath = strPath.Replace('/', chSeparator);
			strPath = strPath.Replace('\\', chSeparator);

			return strPath;
		}
        /// <summary>
        /// 是否以‘\\'或者'//'开头
        /// </summary>
        /// <param name="strPath"></param>
        /// <returns></returns>
		public static bool IsUncPath(string strPath)
		{
			if(strPath == null) throw new ArgumentNullException("strPath");

			return (strPath.StartsWith("\\\\") || strPath.StartsWith("//"));
		}

		public static string FilterFileName(string strName)
		{
			if(strName == null) { Debug.Assert(false); return string.Empty; }

			string str = strName;

			str = str.Replace('/', '-');
			str = str.Replace('\\', '-');
			str = str.Replace(":", string.Empty);
			str = str.Replace("*", string.Empty);
			str = str.Replace("?", string.Empty);
			str = str.Replace("\"", string.Empty);
			str = str.Replace(@"'", string.Empty);
			str = str.Replace('<', '(');
			str = str.Replace('>', ')');
			str = str.Replace('|', '-');

			return str;
		}

	}
}
