﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace DoubleH.Utility
{
    /// <summary>
    /// ChangePwdWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ChangePwdWindow : Window
    {
        public ChangePwdWindow()
        {
            InitializeComponent();
        }

        Table.UserS order;
        public void Init(Table.UserS user)
        {
            if (user == null || user.Id == -1)
            {
                this.Title = "未授权用户";
                buttonSave.IsEnabled = false;
            }
            else
                this.Title = user._LoginName;

            order = user;
            passwordBox1.Focus();
            InitEvent();
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
        }

        private void Save()
        {
            //如果是离线销售模式,这个密码需要更新到后台数据库的
            if (passwordBox1.Password != passwordBox2.Password)
            {
                MessageWindow.Show("密码不一致");
                return;
            }
            order.Password = passwordBox2.Password;
            order.UpdatePassword();
            this.Close();
        }
    }
}