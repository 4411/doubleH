﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoubleH
{
    /// <summary>
    /// SqlError.xaml 的交互逻辑
    /// </summary>
    public partial class SqlError : Window
    {
        public SqlError()
        {
            InitializeComponent();
        }

        public void Init(string message)
        {
            textBlock1.Text = message;

            this.Left = SystemParameters.WorkArea.Width - this.Width;
            this.Top = SystemParameters.WorkArea.Height - this.Height;

            InitTimer();
        }

        private void InitTimer()
        {
            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
            t.Interval = 10000;
            t.Tick += (ss, ee) => { this.Close(); };
            t.Start();
        }

    }
}
