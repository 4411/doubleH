using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility.Translate;
using DoubleH.Utility;
using System.Windows.Controls;

namespace DoubleH.Plugins
{
    /// <summary>
    /// �������(�ӿ�)
    /// </summary>
    public interface IPluginHost
    {
        WorkArea WorkAreaWindow
        {
            get;
        }

        DoubleH.Utility.UC.UCPagePanel PagePanel
        {
            get;
        }

        PluginManager ExtentionManager { get; }
        
        bool CanImport
        {
            set;
        }

        bool CanDelete
        {
            set;
        }
    }
}