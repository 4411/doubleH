﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;
using DoubleH.Utility.Configuration;
using DoubleH.Utility.Translate;
using DoubleH.Plugins;
using System.IO;
using DoubleH.Utility.IO;
using Table = FCNS.Data.Table;

namespace DoubleH
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //base.OnStartup(e);

            bool bRun = true;
            Mutex m = new Mutex(true, "DoubleH.exe", out bRun);
            if (bRun)
            {
                m.Close();
                WorkArea workArea = new WorkArea();
                workArea.Init();
                workArea.Show();
            }
            else
                System.Environment.Exit(0);
        }
    }
}